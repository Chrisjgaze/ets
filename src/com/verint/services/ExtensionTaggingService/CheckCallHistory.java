package com.verint.services.ExtensionTaggingService;

import java.sql.ResultSet;
import java.sql.SQLException;


import com.verint.services.utilites.DeleteInum;
import com.verint.services.utilites.SqlConn;
import com.verint.services.utilites.Utils;
import com.witness.common.wdls.WitLogger;

public class CheckCallHistory {
	
	private static SqlConn dbConn;

	public CheckCallHistory() {	}
	
	public void ProcessCallId(String m_callid, WitLogger m_log) {
		//check SQL
		//if returns record, pass to utils to delete
		m_log.debugHigh("ProcessCallID called with CallID:%s", m_callid);
		String s_sql = "SELECT s.audio_module_no, s.audio_ch_no "
				+ "FROM sessions s INNER JOIN sessions_pd spd ON s.sid = spd.sid "
				+ "WHERE spd.p22_value = '" +m_callid +"'";
		try{
			dbConn = new SqlConn();
			//System.out.println("Connecting to QM Database...");
			m_log.debugHigh("Connecting to QM Database...");
			dbConn.getConnection("DEMUC5AK93","", "", "LocalContact");
			m_log.debugHigh("Executing SQL Statment:%s", s_sql);
			ResultSet rltQMDetails = dbConn.conQuery(s_sql);
			 try {
					while (rltQMDetails.next()) {
						//audio_module_no, audio_ch_no
						m_log.debugHigh("Found audio module: %s with channel no: $s",rltQMDetails.getInt("audio_module_no"),rltQMDetails.getInt("audio_ch_no"));
						int m_serial = rltQMDetails.getInt("audio_module_no");
						int m_id = rltQMDetails.getInt("audio_ch_no");
						m_log.debugHigh("Inum=%s" ,Utils.GetInum(m_serial, m_id));
					 }
				} catch (SQLException e1) {
					m_log.debugHigh("Exception getting details from qm:%s", e1.getMessage());
				}
			//DeleteInum m_deleteinum = new DeleteInum("inum", "recorder", "buffer");
			//Utils.smbDelete("user", "credentilas");
		}
		catch (Exception e){
			System.out.println("Exception caught deleteing inum");
			m_log.debugHigh("Exception caught deleting inum:%s", e.getMessage());
		}
	}
}
