package com.verint.services.ExtensionTaggingService;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;

import com.verint.services.model.CallTracker;
import com.witness.common.threading.GlobalTimerWheel;
import com.witness.common.threading.TimerWheelCommand;
import com.witness.common.wdls.WitLogger;

public class Routiner {
	private final WitLogger m_log = WitLogger.create("Routiner");
	private static TimerWheelCommand m_command;
	
	public Routiner() {
		m_command = GlobalTimerWheel.getTimerWheel().submitRepeatedCommand(new CheckMaps(), 5000L, 10000L);
	}
	private class CheckMaps implements Runnable {
		private CheckMaps() {	}
		public void run() {
			try {
				int mb = 1024*1024;
				//Getting the runtime reference from system
				Runtime runtime = Runtime.getRuntime();
				long i_used = (runtime.totalMemory() - runtime.freeMemory()) / mb;
				long i_free = runtime.freeMemory() / mb;
				long i_total = runtime.totalMemory() / mb;
				long i_max = runtime.maxMemory() / mb;
				
				m_log.debugHigh("Heap utilization statistics [MB]");
				m_log.debugHigh("Used:%s, Free:%s, Total:%s, Max:%s",i_used, i_free, i_total, i_max);
				m_log.debugHigh("Checking map information");
				m_log.debugHigh(ExtensionTaggingService.m_gct.GetMapDetails());
				m_log.debugHigh(CallTracker.GetMapDetails());
				m_log.debugHigh(CallTracker.GetOldestDetails());
				CallTracker.processCalls(m_log);
			}
			catch (Exception e){
				m_log.error("Exception in routiner:%s");
				  Writer writer = new StringWriter();
				  PrintWriter printWriter = new PrintWriter(writer);
				  e.printStackTrace(printWriter);
				  String s = writer.toString();
				  m_log.debug(s);
			}
		}
	}
}
