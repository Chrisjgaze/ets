package com.verint.services.ExtensionTaggingService;

import java.util.HashMap;
import java.util.Map;
import java.util.Timer;

import com.verint.services.adapters.AdapterParser;
import com.verint.services.adapters.eQC.EQCServer;
import com.verint.services.model.CallTracker;
import com.verint.services.model.GenesysCallTracker;
import com.verint.services.recorders.RecorderParser;
import com.witness.common.wdls.WitLogger;

public class ExtensionTaggingService {
	private final static WitLogger m_log = WitLogger.create("ETS");
	//public final static CallTracker m_calltracker = new CallTracker();
	public final static GenesysCallTracker m_gct = new GenesysCallTracker();
//	public final static RecorderParser m_RecorderParser = new RecorderParser();
//	public final static AdapterParser m_adapterparser = new AdapterParser();
	public final static Routiner m_routiner = new Routiner();
	public final static EQCServer m_eQC = new EQCServer();
	public static Timer timer;
	
	public static void main(String[] args) {
		try {
			String s_body = "ETS Service has started";
			//TrapSender.sendMail(s_body, "");
			//TrapSender.sendSnmpV2Trap(1);
			//Thread shutdownThread = new Thread(new ShutdownHandler());
			//Runtime.getRuntime().addShutdownHook(shutdownThread);
	        
			Runnable t_RP = new Thread() {
				public void run() {
					try {
						RecorderParser m_RecorderParser = new RecorderParser();
					} catch (Exception e) {
						m_log.error("Exception in running recorder parser:%s", e.getMessage());
					}
				}
			};
			Thread run_RP = new Thread(t_RP);
			run_RP.start();
			
			Runnable t_AP = new Thread() {
				public void run() {
					try {
						AdapterParser m_adapterparser = new AdapterParser();
					} catch (Exception e) {
						m_log.error("Exception in running adapter parser:%s", e.getMessage());
					}
				}
			};
			Thread run_AP = new Thread(t_AP);
			run_AP.start();	
		} catch (Exception e) {
			m_log.error("Exception in thread main:%s", e.getMessage());
		}
	}
}
