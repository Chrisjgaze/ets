package com.verint.services.alarms;

import java.util.Date;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.snmp4j.CommunityTarget;
import org.snmp4j.PDU;
import org.snmp4j.Snmp;
import org.snmp4j.TransportMapping;
import org.snmp4j.mp.SnmpConstants;
import org.snmp4j.smi.IpAddress;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.OctetString;
import org.snmp4j.smi.UdpAddress;
import org.snmp4j.smi.VariableBinding;
import org.snmp4j.transport.DefaultUdpTransportMapping;

public class TrapSender {

	public static final String community = "public";

	// Sending Trap for sysLocation of RFC1213
	public static final String trapOid = "1.3.6.4.1.1815.1.1.2";
	public static final String oid_Severity = "1.3.6.4.1.1815.1.1.3";
	public static final String oid_Body = "1.3.6.4.1.1815.1.1.4";
	public static final String oid_Name = "1.3.6.4.1.1815.1.1.5";
	public static final String ipAddress = "192.168.2.101";
	public static final int port = 162;

	public static void sendSnmpV2Trap(int err) {
		try {
			// Create Transport Mapping
			TransportMapping transport = new DefaultUdpTransportMapping();
			transport.listen();

			// Create Target
			CommunityTarget comtarget = new CommunityTarget();
			comtarget.setCommunity(new OctetString(community));
			comtarget.setVersion(SnmpConstants.version2c);
			comtarget.setAddress(new UdpAddress(ipAddress + "/" + port));
			comtarget.setRetries(2);
			comtarget.setTimeout(5000);

			// Create PDU for V2
			PDU pdu = getPDU(err);

			// Send the PDU
			Snmp snmp = new Snmp(transport);
			System.out.println("Sending V2 Trap to " + ipAddress + " on Port " + port + " message ::");
			snmp.send(pdu, comtarget);
			snmp.close();
		} catch (Exception e) {
			System.err.println("Error in Sending V2 Trap to " + ipAddress + " on Port " + port);
			System.err.println("Exception Message = " + e.getMessage());
		}
	}

	private static PDU getPDU(int err) {
		PDU pdu = new PDU();
		pdu.add(new VariableBinding(SnmpConstants.sysUpTime, new OctetString(new Date().toString())));
		pdu.add(new VariableBinding(SnmpConstants.snmpTrapOID, new OID(trapOid)));
		pdu.add(new VariableBinding(SnmpConstants.snmpTrapAddress, new IpAddress(ipAddress)));
		pdu.add(new VariableBinding(SnmpConstants.sysObjectID, new OID("1.3.6.1.4.1.1815.1.1.2")));
		switch (err) {
			case 1 : // Service started
				pdu.add(new VariableBinding(new OID(oid_Severity), new OctetString("Minor")));
				pdu.add(new VariableBinding(new OID(oid_Name), new OctetString("eDialler Service has started.")));
				pdu.add(new VariableBinding(new OID(oid_Body), new OctetString("eDiallerStarted")));
				break;
			case 2 : // Service stopped
				pdu.add(new VariableBinding(new OID(oid_Severity), new OctetString("Minor")));
				pdu.add(new VariableBinding(new OID(oid_Name), new OctetString("eDialler Service has stopped.")));
				pdu.add(new VariableBinding(new OID(oid_Body), new OctetString("eDiallerStopped")));
				break;
			case 3 : // Service error
				pdu.add(new VariableBinding(new OID(oid_Severity), new OctetString("Minor")));
				pdu.add(new VariableBinding(new OID(oid_Name), new OctetString("eDialler Service has encountered an error.")));
				pdu.add(new VariableBinding(new OID(oid_Body), new OctetString("eDiallerError")));
				break;
			default :
				pdu.add(new VariableBinding(new OID(oid_Severity), new OctetString("Minor")));
				pdu.add(new VariableBinding(new OID(oid_Name), new OctetString("eDialler error.")));
				pdu.add(new VariableBinding(new OID(oid_Body), new OctetString("eDiallerError")));
				break;
		}
		pdu.setType(PDU.NOTIFICATION);
		return pdu;
	}
	public static void sendMail(String subject, String body) {

		String to = "CCT-SUPPORT@telefonica.com";
		String from = "Ultra_alarm@telefonica.com";
		String host = "10.120.11.1";

		Properties properties = System.getProperties();
		properties.setProperty("mail.smtp.host", host);
		Session session = Session.getDefaultInstance(properties);
		
		try {
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(from));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
			message.setSubject(subject);
			message.setText(body);
			Transport.send(message);
		} catch (MessagingException mex) {
			mex.printStackTrace();
		}

	}
}
