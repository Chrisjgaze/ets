package com.verint.services.model;

import java.util.HashMap;
import java.util.Map;

import com.verint.services.adapters.Genesys.DNStatus;

public class Line {

	private static Map<String, DNStatus> m_line = new HashMap<String, DNStatus>();
	
	public static void addLine(String line, DNStatus status) {
		String s_line = line +" :: " + status.getAdapter();
		m_line.put(s_line, status);
	}
	public static Map<String, DNStatus> getStatus() {
		return m_line;
	}
}
