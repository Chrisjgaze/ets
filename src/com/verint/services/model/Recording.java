package com.verint.services.model;

import java.util.Date;
import java.util.Map;

import com.verint.services.recorders.IPRole;
import com.witness.common.util.DirtyOldMap;
import com.witness.common.util.DirtyOldShortHashMap;

public class Recording {
	public String m_inum;
	private String m_channel;
	private String m_type;
	private String m_primaryInum;
	public Date m_ended;
	public String m_uccid;
	public String m_sipid;
	public boolean m_isBlocked;
	public boolean m_keep = false;
	private DirtyOldMap<String, String> m_attributes = new DirtyOldShortHashMap();
	public IPRole m_role;
	
	public Recording(String inum, String recorder, String type) {
		this.m_inum = inum;
		this.m_channel = inum;
		this.m_type = type;
	}
	public void addAttribute(String key, String value) {
		if ((null == key) || (0 == key.length()) || (null == value) || (0 == value.length())) {
			return;
		}
		this.m_attributes.put(key, value);
		}

	public void addAttributes(Map<String, String> attributes) {
		for (Map.Entry entry : attributes.entrySet())
			addAttribute((String)entry.getKey(), (String)entry.getValue()); 
	}
	public Recording getRecording() {
		return null;
	}
	public String getAttribute(String name){
		String ret = null;
		ret = m_attributes.get(name);
		return ret;
	}
	public Map<String, String> getChangedAttributes() {
		return this.m_attributes.getDirtyMap();
	}
	public void clearChangedAttributes() { this.m_attributes.clearDirtySet(); } 
	public String getAttributeValue(String key) {
		return (String)this.m_attributes.get(key);
	}
	public String toString() {
		String ret= null;
		ret = this.m_inum +" ";
		for (Map.Entry entry : m_attributes.entrySet())
			ret +=entry.getKey() +" " +entry.getValue() + " ";
		return ret;
	}
}
