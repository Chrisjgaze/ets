package com.verint.services.model;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import com.verint.services.model.BlockedCall;
import com.witness.common.threading.GlobalTimerWheel;
import com.witness.common.threading.TimerWheelCommand;

public class BlockedCallTracker {
	
	private HashMap<String, BlockedCall> m_tracker = new HashMap<String, BlockedCall>();
	private TimerWheelCommand m_command;

	public BlockedCallTracker(){
		m_command = GlobalTimerWheel.getTimerWheel().submitRepeatedCommand(new cleanMap(), 5000, 90000);
	}
	public void addCall(String callid, BlockedCall call) {
		if (m_tracker.containsKey(callid))
			m_tracker.remove(callid);
		m_tracker.put(callid, call);
		
	}
	public void removeContact(String callid) {
		try {
			m_tracker.remove(callid);
		}
		catch (Exception e) {
			
		}
	}
	public boolean checkContact(String callid) {
		try {
			if (m_tracker.containsKey(callid))
				return true;
			return false;
		}
		catch (Exception e) {
			return false;
		}
	}
	public String GetMapDetails() {
		String s_ret = "Blocked Call Tracker has :" +m_tracker.size() +"events";
		return s_ret;
	}
	private class cleanMap implements Runnable {
		public void run() {
			try {
				for (Map.Entry kv : m_tracker.entrySet()) {
					BlockedCall contact = m_tracker.get(kv.getKey());
					Date dtCall =  contact.insertedAt;
					Date dtNow = new Date(System.currentTimeMillis());
					long diff = (dtNow.getTime() - dtCall.getTime())*60000;
					if (diff > 120){
						removeContact(contact.globalcallid);
					}
				}
			}
			catch (Exception e) {
				
			}
		}
	}
}
