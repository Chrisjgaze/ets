package com.verint.services.model;

import java.util.Date;
import java.util.Map;

public class GenesysCall {
	public String m_uuid;
	public String m_dn;
	public String m_sipid;
	public String m_pai;
	public Date m_ended;
	public String m_ANI;
	public String m_DNIS;
	public Date m_start;
	
	public GenesysCall(Map<String, String> m_attrs) {
		this.m_uuid = m_attrs.get("uuid");
		this.m_dn = m_attrs.get("dn");
		this.m_sipid = m_attrs.get("sipid");
		this.m_pai = m_attrs.get("pai");
		this.m_ANI = m_attrs.get("ani");
		this.m_DNIS = m_attrs.get("dnis");
		this.m_start = new Date(System.currentTimeMillis());
	}
	public void setAttributes(Map<String, String> m_attrs) {
		this.m_uuid = m_attrs.get("uuid");
		this.m_dn = m_attrs.get("dn");
		this.m_sipid = m_attrs.get("sipid");
		this.m_pai = m_attrs.get("pai");
		this.m_ANI = m_attrs.get("ani");
		this.m_DNIS = m_attrs.get("dnis");
		this.m_start = new Date(System.currentTimeMillis());
	}
	public String print(){
		String ret = "";
		ret = "DN:" +this.m_dn + " SIPID:" +this.m_sipid + " ANI:" +this.m_ANI +"On:" +this.m_start;
		return ret;
	}

}
