package com.verint.services.model;

import java.util.HashMap;
import java.util.Map;

public class Call{
	private String m_inum;
	private String m_uucid;
	private Map<String, String> m_attributes = new HashMap();
	
	public void Call(String inum, Map<String, String> tagKVPs) {
		this.m_inum = inum;
		this.m_attributes = tagKVPs;
	}
}
