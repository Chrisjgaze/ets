package com.verint.services.model;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import com.witness.common.wdls.WitLogger;

public class GenesysCallTracker {

	private HashMap<String, GenesysCall> m_callTracker = new HashMap<String, GenesysCall>();
	private HashMap<String, HashMap<String, GenesysCall>> m_multiCT = new HashMap<String, HashMap<String, GenesysCall>>();
	private HashMap<String, String> m_BySIP = new HashMap<String, String>();
	private HashMap<String, String> m_ByOther = new HashMap<String, String>();
	private HashMap<String, String> m_AniDnis = new HashMap<String, String>();

	public GenesysCallTracker() {
	}

	public void addCall(GenesysCall gcall, WitLogger m_log) {
		try {
			m_log.debugHigh("Adding to tracker DN:%s ; SIPID:%s ; ANI:%s ; DNIS:%s", gcall.m_dn, gcall.m_sipid, gcall.m_ANI, gcall.m_DNIS);
			// SIPID can sometimes be blank....
			m_callTracker.put(gcall.m_dn, gcall);
			if (gcall.m_sipid.length() > 2) {
				if (m_multiCT.containsKey(gcall.m_sipid)) {
					HashMap<String, GenesysCall> m_callList = m_multiCT.get(gcall.m_sipid);
					if (m_callList.containsKey(gcall.m_dn)) {
						// update
					} else {
						m_callList.put(gcall.m_dn, gcall);
						m_multiCT.put(gcall.m_sipid, m_callList);
						m_log.debugHigh("  adding to exsisting MT DN:%s", gcall.m_dn);
					}
				} else {
					//
					HashMap<String, GenesysCall> m_calladd = new HashMap<String, GenesysCall>();
					m_calladd.put(gcall.m_dn, gcall);
					m_multiCT.put(gcall.m_sipid, m_calladd);
					m_log.debugHigh("Creating new MT on  DN:%s", gcall.m_dn);
					m_calladd = null;
				}
			}
			m_ByOther.put(gcall.m_uuid, gcall.m_dn);
			String m_ad = gcall.m_ANI; //
			// m_ad = m_ad + "/" +gcall.m_DNIS; remove DNIS combo, unreliable
			if (!"anonymous".equalsIgnoreCase(m_ad) || m_ad.length() > 2) {
				m_AniDnis.put(m_ad, gcall.m_dn);
				m_log.debugHigh("  adding to ANI/DNIS tracker: %s ; %s", m_ad, gcall.m_dn);
			} else {
				m_log.debugHigh("  Blank / Anonymous value, not adding to tracker");
			}
			m_log.debug("Check if any calls need Extension tagged");
			
			
		} catch (Exception e) {
			m_log.debugHigh("Exception adding call %s to tracker: %s", gcall.m_sipid, e.getMessage());
		}
	}

	public void removeCall(GenesysCall gcall, WitLogger m_log) {
		try {
			m_log.debugHigh("Removing call from tracker DN:%s ; SIPID %s ; ANI:%s ; DNIS %s", gcall.m_dn, gcall.m_sipid, gcall.m_ANI, gcall.m_DNIS);
			m_callTracker.remove(gcall.m_dn);
			if (gcall.m_sipid.length() > 2) {
				if (m_multiCT.containsKey(gcall.m_sipid)) {
					m_log.debugHigh("Remove multicall with id: %s", gcall.m_sipid);
					HashMap<String, GenesysCall> m_callList = m_multiCT.get(gcall.m_sipid);
					if (m_callList.containsKey(gcall.m_dn)) {
						m_log.debugHigh("Remove multicall found DN: %s", gcall.m_dn);
						m_callList.remove(gcall.m_dn);
						if (m_callList.size() <= 0) {
							m_multiCT.remove(gcall.m_sipid);
							m_log.debugHigh("Removed multicall with uuid: %s", gcall.m_sipid);
						} else {
							m_multiCT.remove(gcall.m_sipid);
							m_multiCT.put(gcall.m_sipid, m_callList);
							m_log.debugHigh("GenesysCallTracker still has events for callid:%s", gcall.m_sipid);
						}
					}
				} else {
					m_log.debugHigh("No multi call found to remove");
				}
			}

			m_log.debugHigh("Removing call from Other tracker:%s", gcall.m_uuid);
			m_ByOther.remove(gcall.m_uuid);
			if (!"anonymous".equalsIgnoreCase(gcall.m_ANI) || gcall.m_ANI.length() > 1) {
				//m_log.debugHigh("Removing call from ANI/DNIS tracker:%s", m_ad);
				m_AniDnis.remove(gcall.m_ANI);
			}

		} catch (Exception e) {
			m_log.error("Error removing call: %s", e.getMessage());
		}
	}
	public String getDN(Map<String, String> m_refs, WitLogger m_log) {
		String s_ref = "";
		s_ref = m_refs.get("sipid");
		m_log.debugHigh("Got request for sipid:%s", s_ref);
		String s_return = "";

		GenesysCall m_gcall = null;
		if (m_multiCT.containsKey(s_ref)) {
			m_log.debugHigh("Got match in multiCT");
			HashMap<String, GenesysCall> m_callList = m_multiCT.get(s_ref);
			for (Map.Entry<String, GenesysCall> entry : m_callList.entrySet()) {
				m_gcall = entry.getValue();
				m_log.debugHigh("Found call:%s", m_gcall.print());
				s_return = m_gcall.m_dn;
			}
			m_log.debugHigh("Found call:%s", m_gcall.print());
			return s_return;
		}
		s_ref = m_refs.get("uuid");
		m_log.debugHigh("Call not found by SIP ID, checking UUID:%s", s_ref);
		if (m_ByOther.containsKey(s_ref)) {
			String s_get = m_ByOther.get(s_ref);
			m_gcall = m_callTracker.get(s_get);
			Date m_curDT = new Date(System.currentTimeMillis());
			Date m_callDT = m_gcall.m_start;
			if (((m_curDT.getTime() - m_callDT.getTime()) / 1000) < 2) {
				m_log.debugHigh("Found call:%s", m_gcall.print());
				s_return = m_gcall.m_dn;
				return s_return;
			} else {
				m_log.debugHigh("Found old call in UUID tracker expiring...");
				m_ByOther.remove(s_ref);
			}
		}

		s_ref = m_refs.get("ani");
		m_log.debugHigh("Call not found by UUID, checking by ANI:%s", s_ref);
		if ("anonymous".equalsIgnoreCase(s_ref)) {
			m_log.debugHigh("ANI value is anonymous");
		} else {
			if (m_AniDnis.containsKey(s_ref)) {
				String s_get = m_AniDnis.get(s_ref);
				m_gcall = m_callTracker.get(s_get);
				Date m_curDT = new Date(System.currentTimeMillis());
				Date m_callDT = m_gcall.m_start;
				// original if below
				// if (((m_curDT.getTime() - m_callDT.getTime())/1000) <2) {
				// make search 10 minutes
				if (((m_curDT.getTime() - m_callDT.getTime()) / 1000) < 3600) {
					m_log.debugHigh("Found call:%s", m_gcall.print());
					s_return = m_gcall.m_dn;
					return s_return;
				} else {
					m_log.debugHigh("Found old call in ANI tracker expiring...");
					m_AniDnis.remove(s_ref);
				}
			}
		}
		s_ref = m_refs.get("dnis");
		m_log.debugHigh("Call not found by ANI, checking by DNIS:%s", s_ref);
		if ("anonymous".equalsIgnoreCase(s_ref)) {
			m_log.debugHigh("DNIS value is anonymous");
		} else {
			if (m_AniDnis.containsKey(s_ref)) {
				String s_get = m_AniDnis.get(s_ref);
				m_gcall = m_callTracker.get(s_get);
				Date m_curDT = new Date(System.currentTimeMillis());
				Date m_callDT = m_gcall.m_start;
				// below gives time in seconds
				if (((m_curDT.getTime() - m_callDT.getTime()) / 1000) < 3600) {
					m_log.debugHigh("Found call:%s", m_gcall.print());
					s_return = m_gcall.m_dn;
					return s_return;
				} else {
					m_log.debugHigh("Found old call in DNIS tracker expiring...");
					m_AniDnis.remove(s_ref);
				}
			}
		}
		m_log.debugHigh("Call not found by in any maps");
		return s_return;
	}
	public String GetMapDetails() {
		String s_ret = "MT has:" + m_multiCT.size() + " events, ";
		s_ret = s_ret + "GT has:" + m_callTracker.size() + " events, ";
		s_ret = s_ret + "OT has:" + m_ByOther.size() + " events, ";
		s_ret = s_ret + "ADT has:" + m_AniDnis.size() + " events";
		return s_ret;
	}
}
