package com.verint.services.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import com.witness.common.wdls.WitLogger;

public class CallTracker {
	private final static WitLogger m_log = WitLogger.create();
	private static ConcurrentHashMap<String, Recording> m_calls = new ConcurrentHashMap<String, Recording>();
	private static ConcurrentHashMap<String, Set<Recording>> m_expcalls = new ConcurrentHashMap<String, Set<Recording>>();
	private static HashMap<String, Date> m_dtmap = new HashMap<String, Date>();
	private static HashMap<String, String> m_sipID = new HashMap<String, String>();
	private static Date m_dt1;
	private static List<String> l_inums = new ArrayList<String>();

	public CallTracker() {
		m_dt1 = new Date(System.currentTimeMillis());
	}
	public static void addCall(String ref, Recording recording) {
		m_log.debugHigh("Adding ref:%s to call tracker with inum:%s", ref, recording.m_inum);
		synchronized (m_calls) {
			m_calls.put(ref, recording);
		}
		// add sipid to tracker with inum
		// can then tag if call records before message received from genesys
		m_sipID.put(recording.m_sipid, ref);
	}
	public static void updateCall(String ref, Recording recording) {
		synchronized (m_calls) {
			m_calls.remove(ref);
			m_calls.put(ref, recording);
		}
	}

	public static void removeCall(String ref) {
		l_inums.add(ref);
	}

	private static void removeCallInternal(String ref) {
		m_log.debugHigh("Remove ref:%s from call tracker", ref);
		Recording m_rec = m_calls.get(ref);
		m_log.debugHigh("Found ref:%s from call tracker details:%s", m_rec.m_inum, m_rec.m_sipid);
		String m_sipid = m_rec.m_sipid;
		if (null != m_rec) {
			m_rec.m_ended = new Date(System.currentTimeMillis());
			// m_expcalls.put(m_sipid, m_rec);
			m_dtmap.put(m_sipid, new Date(System.currentTimeMillis()));
			m_log.debugHigh("Adding %s to expired calls", m_sipid);
			//
			Set<Recording> l_rec = new HashSet<Recording>();
			// check if tracker has call already
			if (m_expcalls.containsKey(m_sipid)) {
				l_rec = m_expcalls.get(m_sipid);
			}
			l_rec.add(m_rec);
			m_expcalls.put(m_sipid, l_rec);
		}
		synchronized (m_calls){
			m_calls.remove(ref);
		}
	}
	
	public static void genCheck(String id) {
		String inum = m_sipID.get(id);
		Recording rec = m_calls.get(inum);
		//rec.m_role.
	}
	
	public static boolean keepCall(String inum) {
		boolean keep = false;
		try {
			Recording m_rec = m_calls.get(inum);
			keep = m_rec.m_keep;
		} catch (Exception e) {

		}

		return keep;
	}
	public static List<String> getOldCalls(String s_sipid) {
		m_log.debugHigh("GetOldCalls for sipid:%s", s_sipid);
		List<String> m_list = new ArrayList<String>();
		Recording m_rec = null;
		try {
			if (m_expcalls.containsKey(s_sipid)) {
				m_log.debugHigh("tracker has call");
				Set<Recording> m_recs = m_expcalls.get(s_sipid);
				Iterator<Recording> m_it = null;
				// m_log.debugHigh("getOldCalls has %s calls in set",
				// m_recs.size());
				try {
					m_it = m_recs.iterator();
				} catch (Exception err) {
					m_log.debugHigh("exception in Iterator:%s", err.getMessage());
				}
				while (m_it.hasNext()) {
					m_rec = m_it.next();
					try {
						if (null != m_rec.m_inum) {
							m_log.debugHigh("Adding inum:%s to return list", m_rec.m_inum);
							m_list.add(m_rec.m_inum);
						}
					} catch (Exception e1) {
						m_log.debugHigh("Error in iterator:%s", e1.getMessage());
					}
				}
				m_log.debugHigh("Out of Iterator");
			} else {
				m_log.debugHigh("GetOldCalls did not contain call for sipid:%s", s_sipid);
			}
		} catch (Exception e) {
			m_log.debugHigh("Error in getOldCalls:%s", e.getMessage());
		}
		m_log.debugHigh("Returning list :%s with %s entries", m_list.toString(), m_list.size());
		return m_list;
	}

	public static boolean checkForBlocks(String s_sipid) {
		try {
			m_log.debugHigh("chckForBlockedCalls :%s", s_sipid);
			if (m_expcalls.containsKey(s_sipid)) {
				m_log.debugHigh("checkForBlocks tracker has call");
				Set<Recording> m_recs = m_expcalls.get(s_sipid);
				m_log.debugHigh("checkForBlocks has %s calls in set", m_recs.size());
				Iterator<Recording> m_it = m_recs.iterator();
				Recording m_rec = null;
				while (m_it.hasNext()) {
					m_rec = m_it.next();
					if (m_rec.m_isBlocked) {
						m_log.debugHigh("CheckForBlocks returns True");
						return true;
					}
				}
			}
			m_log.debugHigh("CheckForBlocks returns False");
			return false;
		} catch (Exception e) {
			m_log.debugHigh("Exception in checkForBlockedCalls :%s", e.getMessage());
			return false;
		}
	}
	public static String GetMapDetails() {
		String s_ret = "Expired Call Tracker has:" + m_expcalls.size() + " events, ";
		s_ret = s_ret + "Call Tracker has:" + m_calls.size() + " events, ";
		s_ret = s_ret + "Purge List has:" + m_dtmap.size() + " events";
		return s_ret;
	}
	public static String GetOldestDetails() {
		String s_ret = "Oldest call is:" + m_dt1;
		return s_ret;
	}

	public static void processCalls(WitLogger m_log) {
		try {
			List<String> m_rm = new ArrayList<String>();
			Date m_curDT = new Date(System.currentTimeMillis());

			synchronized (m_dtmap) {
				Iterator<String> m_It = m_dtmap.keySet().iterator();
				while (m_It.hasNext()) {
					String s_id = m_It.next();
					Date m_calldt = m_dtmap.get(s_id);
					// m_log.debugHigh("Current call is:%s Current date is:%s",
					// m_calldt, m_dt1);
					if (m_calldt.getTime() < m_dt1.getTime()) {
						m_dt1 = m_calldt;
					}
					if (((m_curDT.getTime() - m_calldt.getTime()) / 1000) > 36) {
						m_rm.add(s_id);
						m_It.remove();
					}
				}
			}
			Iterator<String> m_rmIt = m_rm.iterator();
			m_log.debugHigh("processCalls has:%s calls to remove", m_rm.size());
			while (m_rmIt.hasNext()) {
				m_expcalls.remove(m_rmIt.next());
			}
			List<String> l_temp = new ArrayList<String>(l_inums);
			l_inums.clear();
			for (String s : l_temp) {
				removeCallInternal(s);
			}
		} catch (Exception e) {
			m_log.error("Exception in processCalls: %s", e.getMessage());
		}
	}
}
