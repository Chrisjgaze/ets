package com.verint.services.utilites;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.witness.common.wdls.WitLogger;

public class SqlConn {
	private final static WitLogger m_log = WitLogger.create();
	private java.sql.Connection con = null;
	private static String strConnString;
	public String strConStatus;
	private String strConnector;

	public SqlConn() {
		this.initialize();
	}
	public java.sql.Connection getConnection(String dbServer, String user, String pass, String dbName) {
		try {
			// Note: this class name changes for ms sql server 2000 thats it
			// It has to match the JDBC library that goes with ms sql 2000
			// Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			strConnector = getConnectionUrl(dbServer, user, pass, dbName);
			DriverManager.setLoginTimeout(5);
			m_log.debug("Class attempted. Connecting to DB with ConString '" + strConnector + "'. Default timeout is '" + DriverManager.getLoginTimeout() + "'.");
			con = DriverManager.getConnection(strConnector);
			System.out.println("Connection processed.");
			if (con != null) {
				m_log.debug("Connection to database <%s> successful", strConnector);
				strConStatus = "Connected";
				return con;
			}
		} catch (Exception e) {
			// e.printStackTrace();
			m_log.error("Exception connecting to datatbase:%s", e.getMessage());
			strConStatus = (e.getMessage());
		}
		return null;
	}
	private void initialize() {
		// this.con = null;
	}
	private String getConnectionUrl(String dbServer, String user, String pass, String dbName) {
		if ("".equalsIgnoreCase(user)) {
			strConnString = "jdbc:sqlserver://" + dbServer + ";databaseName=" + dbName + ";integratedSecurity=true;";
		} else {
			strConnString = "jdbc:sqlserver://" + dbServer + ";user=" + user + ";" + "password=" + pass + ";databaseName=" + dbName + ";";
		}
		return strConnString;
	}
	public Boolean chkConn() {
		try {
			if (con.getCatalog().toString().equals("")) {
				return false;
			} else
				return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} catch (NullPointerException en) {
			en.printStackTrace();
			return false;
		}
	}
	public ResultSet conQuery(String strSQL) {
		// conLocal = this.getLocalConnection();
		Statement SelectMaxInum = null;
		try {
			SelectMaxInum = con.createStatement();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			ResultSet rltInum = SelectMaxInum.executeQuery(strSQL);
			return rltInum;

		} catch (SQLException e) {

			e.printStackTrace();
		}
		return null;
	}
	public Boolean dbExecSP(String strSQL) throws SQLException {
		con.createStatement().execute(strSQL);
		return true;
	}

	public void closeConnection() {
		try {
			if (con != null)
				con.close();
			con = null;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
