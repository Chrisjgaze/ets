package com.verint.services.utilites;

class WatcherDataHolder
{
  String m_fileName;
  long m_lastModified;
  FileWatcher.IFileWatcherCallBack m_callBack;

  public WatcherDataHolder(String fileName, long lastmodified, FileWatcher.IFileWatcherCallBack callback)
  {
    this.m_fileName = fileName;
    this.m_lastModified = lastmodified;
    this.m_callBack = callback;
  }
}