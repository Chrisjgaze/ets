package com.verint.services.utilites;

import com.witness.common.threading.AsyncTimerWheelCommand;
import com.witness.common.threading.GlobalTimerWheel;
import java.io.File;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class FileWatcher
{
  private static Map<String, WatcherDataHolder> m_fileMap = Collections.synchronizedMap(new HashMap());
  private static Map<Object, Map<String, WatcherDataHolder>> m_manualMap = Collections.synchronizedMap(new HashMap());
  private static RecheckMe m_checkme = new RecheckMe();
  private static AsyncTimerWheelCommand mh_checkMe = null;

  public static boolean addFileToWatch(String strFile, IFileWatcherCallBack callback)
  {
    if (callback == null) return false;

    File file = ConfigurationUtility.getConfigurationFile(strFile);

    if (m_fileMap.size() == 0) {
      mh_checkMe = new AsyncTimerWheelCommand(m_checkme);
      GlobalTimerWheel.getTimerWheel().submitRepeatedCommand(mh_checkMe, new Date(System.currentTimeMillis()), 15000L);
    }

    m_fileMap.put(strFile, new WatcherDataHolder(strFile, file == null ? 0L : file.lastModified(), callback));

    return true;
  }

  public static boolean removeFileToWatch(String strFile) {
    if (m_fileMap.remove(strFile) == null) return false;

    if ((m_fileMap.size() == 0) && (mh_checkMe != null)) {
      mh_checkMe.cancelCommand();
      mh_checkMe = null;
    }

    return true;
  }

  public static boolean fileChanged(Object checker, String strFile)
  {
    Map map = (Map)m_manualMap.get(checker);
    if (null == map) {
      map = new HashMap();
      m_manualMap.put(checker, map);
    }

    WatcherDataHolder holder = (WatcherDataHolder)map.get(strFile);
    File file = ConfigurationUtility.getConfigurationFile(strFile);

    if (null == holder) {
      map.put(strFile, new WatcherDataHolder(strFile, file == null ? 0L : file.lastModified(), null));
      return true;
    }

    if ((null == file) || (!file.exists())) {
      holder.m_lastModified = 0L;
      return true;
    }

    long modified = file.lastModified();
    if (holder.m_lastModified != modified) {
      holder.m_lastModified = modified;
      return true;
    }

    return false;
  }

  public static abstract interface IFileWatcherCallBack
  {
    public abstract void fileModified();
  }

  static class RecheckMe
    implements Runnable
  {
    public void run()
    {
      for (String fileName : FileWatcher.m_fileMap.keySet()) {
        WatcherDataHolder holder = (WatcherDataHolder)FileWatcher.m_fileMap.get(fileName);
        File t_file = ConfigurationUtility.getConfigurationFile(fileName);

        if ((t_file == null) || (!t_file.exists())) {
          holder.m_callBack.fileModified();
          continue;
        }

        if (t_file.isDirectory())
          continue;
        long modifedTime = t_file.lastModified();
        if (holder.m_lastModified != modifedTime) {
          holder.m_callBack.fileModified();
          holder.m_lastModified = modifedTime;
        }
      }
    }
  }
}