package com.verint.services.utilites;

import com.witness.common.threading.GlobalTimerWheel;
import com.witness.common.threading.TimerWheel;
import com.witness.common.threading.TimerWheelCommand;
import com.witness.common.util.ArraySet;
import com.witness.common.util.MultiHashMap;
import com.witness.common.util.XMLHelper;
import com.witness.common.wdls.WitLogger;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class ManifestWatcher implements FileWatcher.IFileWatcherCallBack {
	private static WitLogger m_log = WitLogger.create();

	private static ManifestWatcher m_manifestWatch = null;
	private Map<IManifestWatcherCallBack, Set<String>> m_callBacks = new HashMap();
	private static Set<String> m_watchedFileCategories = new HashSet();
	private Map<String, FileDataHolder> m_manifestEntries = new HashMap();
	private ManifestUpdate m_currentUpdate = new ManifestUpdate();
	private static String m_hintDirectory = null;

	private TimerWheelCommand m_retry = null;

	private static int m_sequence = 0;

	public static int getSequence() {
		return m_sequence;
	}

	public void fileModified() {
		try {
			String manifestName = "Cache-Manifest.xml";
			File file = ConfigurationUtility.getConfigurationFile(manifestName,
					m_hintDirectory);
			if (null == file)
				return;
			m_hintDirectory = file.getParent();

			Document doc = XMLHelper.readXMLFile(manifestName, file);
			if (null == doc)
				return;

			Element root = doc.getDocumentElement();
			m_sequence = XMLHelper.getAttributeInt(root, "Sequence");
			List entries = XMLHelper.getElementsByName(root, "Entry");

			ManifestUpdate update = new ManifestUpdate();

			Set oldFiles = new HashSet(this.m_manifestEntries.keySet());
/**
			for (Element entry : entries) {
				String fileName = entry.getAttribute("Name");
				String category = entry.getAttribute("Type");
				long modified = XMLHelper
						.getAttributeLong(entry, "LastUpdated");

				FileDataHolder fileData = (FileDataHolder) this.m_manifestEntries
						.get(fileName);
				if (null == fileData) {
					fileData = new FileDataHolder(category, fileName, modified);
					this.m_manifestEntries.put(fileName, fileData);
					update.setFile(category, Operation.Added, fileName);
				} else {
					if (modified != fileData.m_time) {
						fileData.m_time = modified;
						update.setFile(category, Operation.Modified, fileName);
					}

					oldFiles.remove(fileName);
				}

			}

			for (String fileName : oldFiles) {
				FileDataHolder fileData = (FileDataHolder) this.m_manifestEntries
						.remove(fileName);
				update.setFile(fileData.m_type, Operation.Deleted, fileName);
			}

			for (String fileName : this.m_manifestEntries.keySet()) {
				FileDataHolder fileData = (FileDataHolder) this.m_manifestEntries
						.get(fileName);
				update.setFile(fileData.m_type, Operation.Remained, fileName);
			}
			**/
			this.m_currentUpdate = update;
			performCallBacks(update, this.m_callBacks.keySet());
		} catch (Exception ex) {
			m_log.error("Exception while reading Manifest file: %s", ex,
					new Object[]{ex});
		}
	}

	private void performCallBacks(ManifestUpdate update,
			Collection<IManifestWatcherCallBack> callBacks) {
		if (null == update)
			return;

		synchronized (m_manifestWatch) {
			try {
				if (!update.lock()) {
					m_log.error("Failed to lock all files, rescheduling update");
					/**
					if (null == this.m_retry) {
						this.m_retry = GlobalTimerWheel.getTimerWheel()
								.submitRepeatedCommand(
										new TimerWheelCommand(new RetryUpdate(
												null)), 500L, 5000L);
					}
					**/
					update.unlock();
					return;
				}
				if (null != this.m_retry) {
					this.m_retry.cancelCommand();
					this.m_retry = null;
				}

				for (IManifestWatcherCallBack callback : callBacks)
					try {
						callback.manifestModifiedMessage(update);
					} catch (Exception e) {
						m_log.error("Failed to process Manifest callback: %s",
								e, new Object[]{e});
					}
			} finally {
				update.unlock();
			}
		}
	}

	public static void watch(IManifestWatcherCallBack callback,
			Set<String> watchedFileCategories) {
		if (null == m_manifestWatch) {
			m_manifestWatch = new ManifestWatcher();
			m_manifestWatch.fileModified();
			FileWatcher.addFileToWatch("Cache-Manifest.xml", m_manifestWatch);
		}

		synchronized (m_manifestWatch.m_callBacks) {
			m_manifestWatch.m_callBacks.put(callback, watchedFileCategories);
			rebuildWatchedFileCategories();
		}

		Collection callbacks = new ArraySet(callback);
		m_manifestWatch.performCallBacks(m_manifestWatch.m_currentUpdate,
				callbacks);
	}

	private static void rebuildWatchedFileCategories() {
		m_watchedFileCategories = new HashSet();
		for (Set categories : m_manifestWatch.m_callBacks.values())
			m_watchedFileCategories.addAll(categories);
	}

	public static boolean removeWatch(IManifestWatcherCallBack callback) {
		if (null == m_manifestWatch)
			return false;

		synchronized (m_manifestWatch.m_callBacks) {
			boolean removed = null != m_manifestWatch.m_callBacks
					.remove(callback);
			rebuildWatchedFileCategories();
			return removed;
		}
	}

	public static enum Operation {
		Added, Modified, Deleted, Remained;
	}

	class FileDataHolder {
		String m_type;
		String m_fileName;
		long m_time;

		FileDataHolder(String type, String fileName, long time) {
			this.m_type = type;
			this.m_fileName = fileName;
			this.m_time = time;
		}
	}

	public static class ManifestUpdate {
		private Map<String, ManifestWatcher.CategoryUpdate> m_categories = new HashMap();

		private void setFile(String category,
				ManifestWatcher.Operation operation, String fileName) {
			ManifestWatcher.CategoryUpdate categoryUpdate = (ManifestWatcher.CategoryUpdate) this.m_categories
					.get(category);
			if (null == categoryUpdate) {
				//categoryUpdate = new ManifestWatcher.CategoryUpdate(null);
				this.m_categories.put(category, categoryUpdate);
			}

			categoryUpdate.put(operation, fileName);
		}

		public boolean lock() {
			for (Map.Entry entry : this.m_categories.entrySet()) {
				synchronized (ManifestWatcher.m_manifestWatch.m_callBacks) {
					if (!ManifestWatcher.m_watchedFileCategories.contains(entry
							.getKey()))
						continue;
				}
				if (!((ManifestWatcher.CategoryUpdate) entry.getValue()).lock())
					return false;
			}
			return true;
		}

		public void unlock() {
			for (ManifestWatcher.CategoryUpdate catUpdate : this.m_categories
					.values())
				catUpdate.unlock();
		}

		public Set<ManifestWatcher.FileUpdate> getFiles(String category,
				ManifestWatcher.Operation operation) {
			ManifestWatcher.CategoryUpdate categoryUpdate = (ManifestWatcher.CategoryUpdate) this.m_categories
					.get(category);
			if (null == categoryUpdate) {
				return new HashSet();
			}
			Set retval = categoryUpdate.get(operation);
			if (null == retval)
				retval = new HashSet();
			return retval;
		}
		public int getSequenceId() {
			return ManifestWatcher.m_sequence;
		}
		public void debug(WitLogger log) {
			if (this.m_categories.size() == 0) {
				return;
			}
			log.debug(
					"Manifest Update <%s>",
					new Object[]{Integer.valueOf(ManifestWatcher.getSequence())});
			for (Map.Entry entry : this.m_categories.entrySet()) {
				log.debug("%s:", new Object[]{entry.getKey()});
				((ManifestWatcher.CategoryUpdate) entry.getValue()).debug(log);
			}
		}
	}

	private static class CategoryUpdate {
		MultiHashMap<ManifestWatcher.Operation, ManifestWatcher.FileUpdate> m_updates = new MultiHashMap();

		void put(ManifestWatcher.Operation operation, String fileName) {
			this.m_updates.put(operation, new ManifestWatcher.FileUpdate(
					fileName));
		}

		boolean lock() {
			if (!lock(get(ManifestWatcher.Operation.Added)))
				return false;
			if (!lock(get(ManifestWatcher.Operation.Modified)))
				return false;
			return lock(get(ManifestWatcher.Operation.Remained));
		}

		boolean lock(Set<ManifestWatcher.FileUpdate> fileUpdates) {
			if (null == fileUpdates)
				return true;
			for (ManifestWatcher.FileUpdate fileUpdate : fileUpdates)
				if (!fileUpdate.lock())
					return false;
			return true;
		}

		void unlock() {
			unlock(get(ManifestWatcher.Operation.Added));
			unlock(get(ManifestWatcher.Operation.Modified));
			unlock(get(ManifestWatcher.Operation.Remained));
		}

		void unlock(Set<ManifestWatcher.FileUpdate> fileUpdates) {
			if (null == fileUpdates)
				return;
			for (ManifestWatcher.FileUpdate fileUpdate : fileUpdates)
				fileUpdate.unlock();
		}

		Set<ManifestWatcher.FileUpdate> get(ManifestWatcher.Operation operation) {
			return this.m_updates.get(operation);
		}
		public void debug(WitLogger log) {
			for (Map.Entry opEntry : this.m_updates.entrySet())
				log.debug("  %s %s",
						new Object[]{opEntry.getKey(), opEntry.getValue()});
		}
	}

	public static class FileUpdate {
		String m_fileName;
		File m_file;
		FileInputStream m_fileInputStream;

		FileUpdate(String fileName) {
			this.m_fileName = fileName;
		}
		boolean lock() {
			if (null != this.m_fileInputStream)
				return true;

			this.m_file = ConfigurationUtility.getConfigurationFile(
					this.m_fileName, ManifestWatcher.m_hintDirectory);
			if ((null == this.m_file) || (!this.m_file.exists())) {
				ManifestWatcher.m_log.error(
						"Could not find file for reading<%s>",
						new Object[]{this.m_fileName});
				return false;
			}
			try {
				this.m_fileInputStream = new FileInputStream(this.m_file);
				return true;
			} catch (FileNotFoundException e) {
				ManifestWatcher.m_log.error(
						"Could not open file for reading<%s> %s", e,
						new Object[]{this.m_file.getPath(), e});
			}
			return false;
		}

		void unlock() {
			if (null != this.m_fileInputStream) {
				try {
					this.m_fileInputStream.close();
				} catch (Exception e) {
				}
				this.m_fileInputStream = null;
			}

			if (null != this.m_file)
				this.m_file = null;
		}

		public String toString() {
			return this.m_fileName;
		}
		public String getFileName() {
			return this.m_fileName;
		}
		public FileInputStream getFileInputStream() {
			return this.m_fileInputStream;
		}
		public File getFile() {
			return this.m_file;
		}
	}

	public static abstract interface IManifestWatcherCallBack {
		public abstract void manifestModifiedMessage(
				ManifestWatcher.ManifestUpdate paramManifestUpdate);
	}

	private class RetryUpdate implements Runnable {
		private RetryUpdate() {
		}

		public void run() {
			ManifestWatcher.this.performCallBacks(
					ManifestWatcher.this.m_currentUpdate,
					ManifestWatcher.this.m_callBacks.keySet());
		}
	}
}