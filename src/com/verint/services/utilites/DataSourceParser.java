package com.verint.services.utilites;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import com.witness.common.util.XMLHelper;

public class DataSourceParser { 
	
	private List<String> m_extns = new ArrayList<String>(); 
	
	public List<String> parse(int ds) {
		m_extns.clear();
		String filename = "DataSource-" +ds +".xml";
		File file = ConfigurationUtility.getConfigurationFile(filename);
	    Document doc = XMLHelper.readXMLFile(filename, file);
	    if (null == doc)
	    	return null;
	    Element root = doc.getDocumentElement();
	    Element dataports = XMLHelper.getElementByName(root, "DataPorts");
	    handledataports(dataports, "");
	    return m_extns;   
	}
	private void handledataports(Element site, String indent) {
		for (Element dp : XMLHelper.getElementsByName(site, "DataPort")) {
			handleExtension(dp, indent + "  ");
		}
	}
	private void handleExtension(Element node, String indent) {
		String s_extn = node.getAttribute("Value");
		//System.out.println("Adding:" +s_extn);
		m_extns.add(s_extn);
	}
}
