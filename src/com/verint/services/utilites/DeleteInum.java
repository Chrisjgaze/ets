package com.verint.services.utilites;

import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import jcifs.smb.NtlmPasswordAuthentication;

import com.witness.common.wdls.WitLogger;



public class DeleteInum {
	
	public DeleteInum() {}

	public void DelInum(String m_inum, String m_recorder, WitLogger m_log) {
		try {
			m_log.debugHigh("Delete inum:%s", m_inum);
			//String user = txtUserName.getText() +":" +txtPass.getText();
			String s_dom = "viaginterkom";
			String s_user = "s_msaqmtprod";
			String s_pwd = "TofBeuTwh62qvCQu";
			NtlmPasswordAuthentication auth = new NtlmPasswordAuthentication(s_dom, s_user, s_pwd);
			m_log.debugHigh("user:%s",auth.getUsername());
			m_log.debugHigh("domain:%s",auth.getDomain());
			m_log.debugHigh("pass:%s",auth.getPassword());
			if (m_inum.contains("861004"))
				m_recorder = "DEWSTIAK02";
			if (m_inum.contains("861003"))
				m_recorder = "DEWSTIAK01";
		    String path = "smb://" +m_recorder +"/G$/Calls/" +Utils.getINUMPath(m_inum) +".wav";
		    m_log.debugHigh("Delete inum from path:%s", path);
		    String s_result = Utils.smbDelete(path,auth);
		    m_log.debugHigh("Delete function returns:%s", s_result);
		    //DeleteContact(m_inum, m_log);
			String s_sid = "";
			String s_contactID = "";
			String m_audio_module_no = m_inum.substring(0,6);
			String m_audio_ch_no = m_inum.substring(6);
			List<Integer> l_sids = new ArrayList<Integer>();
			List<String> l_cids = new ArrayList<String>();
			Date m_date = new Date(System.currentTimeMillis());
			SimpleDateFormat sdf = new SimpleDateFormat("M");
			String i_month = sdf.format(m_date);
			m_audio_ch_no = m_audio_ch_no.replaceFirst ("^0*", "");
			m_log.debugHigh("Module:%s Channel:%s", m_audio_module_no, m_audio_ch_no);
			System.out.println("module:" +m_audio_module_no +" channel:" +m_audio_ch_no);
			String s_query = "SELECT sid, contact_id FROM SESSIONS_MONTH_" + i_month +" "
					+ "WHERE audio_module_no = " +m_audio_module_no  +" "
					+ "AND audio_ch_no = " +m_audio_ch_no;
			m_log.debugHigh(s_query);
			SqlConn m_sql = new SqlConn();
			m_sql.getConnection("DEMUC5AK93\\IMPACT360:1433", "", "","CentralContact");
			ResultSet rs = m_sql.conQuery(s_query);
			
			if (rs.next()) {
				l_sids.add(rs.getInt(1));
				l_cids.add(rs.getString(2));
			}
			if (l_cids.isEmpty()) {
				m_log.debugHigh("No detials found for inum:%s", m_inum);
				return;
			}
			String s_add = "";
			String s_delContact = "DELETE FROM Contacts WHERE contact_id IN(";
			for (String s : l_cids) {
				if (s_add.length() > 0 ) {
					s_add = s_add +",'" +s +"'";
				}else {
					s_add = s_add +"'" +s +"'";
				}
			}
			s_delContact = s_delContact +s_add +");";
			m_log.debugHigh("DeleteContact: %s", s_delContact);
			s_add = "";
			String s_delSessions = "DELETE FROM Sessions WHERE sid IN(";
			
			for (Integer i : l_sids) {
				if (s_add.length() >1) {
					s_add = s_add +", " +i;
				}else {
					s_add = s_add +i;
				}
			}
			s_delSessions = s_delSessions +s_add +");";
			m_log.debugHigh("DeleteSessions: %s", s_delSessions);
			s_add = "";
			String s_delPD = "DELETE FROM PD WHERE sid IN(";
			for (Integer i : l_sids) {
				if (s_add.length() >1) {
					s_add = s_add +", " +i;
				}else {
					s_add = s_add +i;
				}
			}		
			s_delPD = s_delPD +s_add +");";		
			m_log.debugHigh("DeletePD: %s", s_delPD);
			m_sql.closeConnection();
			m_sql = null;
		}
		catch (Exception e) {
			m_log.debugHigh("Exception Deleting inum:%s",e.getMessage());
		}
	}
	
	private void DeleteContact(String m_inum, WitLogger m_log) throws Exception{
		String s_sid = "";
		String s_contactID = "";
		String m_audio_module_no = m_inum.substring(0,6);
		String m_audio_ch_no = m_inum.substring(6);
		List<Integer> l_sids = new ArrayList<Integer>();
		List<String> l_cids = new ArrayList<String>();
		Date m_date = new Date(System.currentTimeMillis());
		SimpleDateFormat sdf = new SimpleDateFormat("M");
		String i_month = sdf.format(m_date);
		m_audio_ch_no = m_audio_ch_no.replaceFirst ("^0*", "");
		System.out.println("module:" +m_audio_module_no +" channel:" +m_audio_ch_no);
		String s_query = "SELECT sid, contactid FROM SESSIONS_MONTH_" + i_month +" "
				+ "WHERE audio_module_no = " +m_audio_module_no  +" "
				+ "AND audio_ch_no = " +m_audio_ch_no;
	
		SqlConn m_sql = new SqlConn();
		m_sql.getConnection("db", "", "","CentralContact");
		ResultSet rs = m_sql.conQuery(s_query);
		
		if (rs.next()) {
			l_sids.add(rs.getInt(1));
			l_cids.add(rs.getString(2));
		}
		String s_add = "";
		String s_delContact = "DELETE FROM Contacts WHERE contactid IN(";
		for (String s : l_cids) {
			if (s_add.length() > 0 ) {
				s_add = s_add +",'" +s +"'";
			}else {
				s_add = s_add +"'" +s +"'";
			}
		}
		s_delContact = s_delContact +s_add +");";
		m_log.debugHigh("DeleteContact: %s", s_delContact);
		s_add = "";
		String s_delSessions = "DELETE FROM Sessions WHERE sid IN(";
		
		for (Integer i : l_sids) {
			if (s_add.length() >1) {
				s_add = s_add +", " +i;
			}else {
				s_add = s_add +i;
			}
		}
		s_delSessions = s_delSessions +s_add +");";
		m_log.debugHigh("DeleteSessions: %s", s_delSessions);
		s_add = "";
		String s_delPD = "DELETE FROM PD WHERE sid IN(";
		for (Integer i : l_sids) {
			if (s_add.length() >1) {
				s_add = s_add +", " +i;
			}else {
				s_add = s_add +i;
			}
		}		
		s_delSessions = s_delSessions +s_add +");";		
		m_log.debugHigh("DeletePD: %s", s_delPD);
		m_sql.closeConnection();
		m_sql = null;
	}
}
