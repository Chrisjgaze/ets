package com.verint.services.utilites;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import com.witness.common.wdls.WitLogger;

public class MySQL {
	private final WitLogger m_log = WitLogger.create("MySQL");;
    private java.sql.Connection con = null;
    private static String strConnString;
    public	String strConStatus;
    private String strConnector;
 
    
    public MySQL(){
    	this.initialize();
    }
    public java.sql.Connection getConnection(String dbServer, String user, String pass, String dbName) {
        try {
        	    this.m_log.info("Building sqlCon class...");
        	    Class.forName("com.mysql.jdbc.Driver");
        	    strConnString = "jdbc:mysql://" + dbServer + "/" + dbName;
        	    DriverManager.setLoginTimeout(5);
        	    System.out.println("conn string is:" + strConnString);
        	    this.m_log.debug("Connecting to: %s", strConnString);
                con = DriverManager.getConnection(strConnString, user, pass);
                this.m_log.info("Connection processed.");
                if (con != null){
                	if (chkConn()){
                		this.m_log.info("Connection to Database Successful!");
                		System.out.println("MySQL Conn");
                        strConStatus = "Connected";
                        return con;
                	}
                	else{
                		this.m_log.info("Connection failed");
                		strConStatus = "Disconnected";
                		return con;
                	}
                }
        } catch (Exception e) {
                //e.printStackTrace();
        	this.m_log.info("Error Trace in getConnection() : "+ e.getMessage());
            strConStatus = (e.getMessage());
        }
        return null;
    }
	private void initialize(){
	}
	public Boolean chkConn(){
		try {
			if (con.getCatalog().toString().equals("")){
				System.out.println(con.getCatalog().toString());
				return false;
			}
			else
				return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} catch (NullPointerException en) {
			en.printStackTrace();
			return false;
		}
	}
	    public ResultSet conQuery (String strSQL){
    		Statement SelectMaxInum = null;
			try {
				SelectMaxInum = con.createStatement();
			} catch (SQLException e) {
				e.printStackTrace();
			}
    		try {
				ResultSet rltInum = SelectMaxInum.executeQuery(strSQL);
				return rltInum;
				
			} catch (SQLException e) {

				e.printStackTrace();
			}
			return null;
	    }
	    public Boolean dbExecSP(String strSQL) throws SQLException{
    		con.createStatement().execute(strSQL);
			return true;
	    }
	    
	    public void closeConnection() {
	        try {
	                if (con != null)
	                        con.close();
	                con = null;
	        } catch (Exception e) {
	                e.printStackTrace();
	        }
}
}


