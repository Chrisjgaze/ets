package com.verint.services.utilites;

import com.witness.common.wdls.WitLogger;

import jcifs.smb.NtlmPasswordAuthentication;
import jcifs.smb.SmbFile;

public class Utils {
	private final WitLogger m_log = WitLogger.create();
	
	  public String[] cSStoArray(String aString){
		    String[] splittArray = null;
		    if (aString != null && !aString.equalsIgnoreCase("")){
		         splittArray = aString.split(",");
		    }
		    return splittArray;
		}
	  public String[] trimString(String[] aString){
       if(aString != null)
       {
      	 for (int i = 0; i < aString.length; i++) {
      		 aString[i] = aString[i].trim();
			}
       }
       return aString;
	  }
	  public static String getINUMPath(String Inum){
		  String IPath = Inum.substring(0,6) +"/";
		  IPath = IPath + Inum.substring(6,9) +"/";
		  IPath = IPath + Inum.substring(9,11) +"/";
		  IPath = IPath + Inum.substring(11,13) +"/";
		  IPath = IPath + Inum;
		  return IPath;
	  }
	  public static String GetInum(int m_serial, int m_id) {
		  //todo
		  int m_slen = Integer.toString(m_serial).length();
		  int m_idlen = Integer.toString(m_id).length();
		  int m_pad = 15 - (m_slen + m_idlen);
		  String s_pad = "";
		  for (int i=0; i<m_pad; i++) {
			  s_pad = s_pad +"0";
		  }
		  String s_ret = Integer.toString(m_serial) +s_pad +Integer.toString(m_id);
		  return s_ret;
	  }
	  @SuppressWarnings("unused")
	  public static String smbDelete(String path, NtlmPasswordAuthentication Credentials) throws Exception{
		  
		  SmbFile sFile = null;
		  sFile = new SmbFile(path, Credentials);
		  if (sFile.exists()){
			  sFile.delete();
			  return("Sucess");
		  }else {
			  return("File not found: " + path);

		  }
	  }
}


