package com.verint.services.utilites;

import java.io.File;
import java.net.URL;
import java.security.CodeSource;
import java.security.ProtectionDomain;

public class ConfigurationUtility {
	
	  private static File findFile(String location, String fileName)
	  {
	    File file = new File(location + File.separator + fileName);
	    if (file.exists()) return file;
	    return null;
	  }
	  public static File getConfigurationFile(String fileName) {
	    return getConfigurationFile(fileName, null);
	  }
	  public static File getConfigurationFile(String fileName, String hintPath) {
	    if ((fileName.contains("/")) || (fileName.contains("\\"))) {
	      File file = new File(fileName);
	      if (file.exists()) return file;
	    }

	    if (null != hintPath) {
	      File retval = findFile(hintPath, fileName);
	      if (retval != null) return retval;

	    }

	    File retval = findFile("../../conf/cache", fileName);
	    if (retval != null) return retval;

	    retval = findFile("../../conf/roles", fileName);
	    if (retval != null) return retval;

	    retval = findFile("../../conf/roles/INTEGRATION_FRAMEWORK", fileName);
	    if (retval != null) return retval;

	    retval = findFile("../../conf/alarm", fileName);
	    if (retval != null) return retval;

	    retval = findFile("../../conf", fileName);
	    if (retval != null) return retval;

	    retval = findFile("../conf/cache", fileName);
	    if (retval != null) return retval;

	    retval = findFile("../conf/roles", fileName);
	    if (retval != null) return retval;

	    retval = findFile("../conf/roles/INTEGRATION_FRAMEWORK", fileName);
	    if (retval != null) return retval;

	    retval = findFile("../conf/alarm", fileName);
	    if (retval != null) return retval;

	    retval = findFile("../conf", fileName);
	    if (retval != null) return retval;
	    
	    retval = findFile("",fileName);
	    if (retval != null) return retval;
	    
	    URL location = ConfigurationUtility.class.getProtectionDomain().getCodeSource().getLocation();
	    if (location != null) {
	      String path = location.getPath();
	      if (path != null) {
	        retval = findFile(path + "/../../conf/cache", fileName);
	        if (retval != null) return retval;

	        retval = findFile(path + "/../../conf/roles", fileName);
	        if (retval != null) return retval;

	        retval = findFile(path + "/../../conf/roles/INTEGRATION_FRAMEWORK", fileName);
	        if (retval != null) return retval;

	        retval = findFile(path + "/../../conf/alarm", fileName);
	        if (retval != null) return retval;

	        retval = findFile(path + "/../../conf", fileName);
	        if (retval != null) return retval;

	        retval = findFile(path + "/../conf/cache", fileName);
	        if (retval != null) return retval;

	        retval = findFile(path + "/../conf/roles", fileName);
	        if (retval != null) return retval;

	        retval = findFile(path + "/../conf/roles/INTEGRATION_FRAMEWORK", fileName);
	        if (retval != null) return retval;

	        retval = findFile(path + "/../conf/alarm", fileName);
	        if (retval != null) return retval;

	        retval = findFile(path + "/../conf", fileName);
	        if (retval != null) return retval;

	        retval = findFile(path, fileName);
	        if (retval != null) return retval;
	      }
	    }

	    return findFile(".", fileName);
	  }
}

