package com.verint.services.adapters.eQC;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.concurrent.Executors;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import com.witness.common.wdls.WitLogger;


public class EQCServer {
	private final WitLogger m_log = WitLogger.create("eQCServer");
	public EQCServer() {
		try {
			m_log.debugHigh("Start eQCServer on port 3040");
			InetSocketAddress addr = new InetSocketAddress(3040);
			HttpServer server = HttpServer.create(addr, 0);
			server.createContext("/", new MyHandler());
			server.setExecutor(Executors.newCachedThreadPool());
			server.start();
			m_log.debugHigh("Server is listening on port 3040");
		} catch (Exception e) {
			
		}
	}
}

class MyHandler implements HttpHandler {
	private final WitLogger m_log = WitLogger.create("eQCServer");
	public void handle(HttpExchange exchange) throws IOException {
		String requestMethod = exchange.getRequestMethod();
		String s_response = "";
		if (requestMethod.equalsIgnoreCase("GET")) {
			Headers responseHeaders = exchange.getResponseHeaders();
			responseHeaders.set("Content-Type", "text/html");
			responseHeaders.add("Content-type", "text/html");
			//exchange.sendResponseHeaders(200, 0);

			OutputStream responseBody = exchange.getResponseBody();

			if("/Status".equalsIgnoreCase(exchange.getRequestURI().toString())) {
				System.out.println("status request");
				Dashboard m_dash = new Dashboard();
				s_response = m_dash.Response(); 
			}
			else {
				m_log.debugHigh("UnHandled");
			}
			System.out.println(s_response);
			exchange.sendResponseHeaders(200, s_response.getBytes().length);
			responseBody.write(s_response.getBytes());
			responseBody.close();
		}
	}
	//http://localhost:3030/servlet/eQC6?interface=IAgentManagement&method=deliverevent&agentevent=AgentLogon&device.device=UK-CGAZE7-LP2&agent.agent=CGAZE
	private String handleRequest(String s_req) {
		m_log.debugHigh("Agent Interface: %s", s_req);
		System.out.println("Agent Interface: " +s_req);
		String s_del = "=";
		s_req = s_req.substring(14);
		s_req = "&" +s_req;
		HashMap<String, String> m_kvps = new HashMap<String, String>();
		StringTokenizer st = new StringTokenizer(s_req, "&;"); 
		while(st.hasMoreTokens()) { 
			String kvp[] = st.nextToken().split(s_del);  
			System.out.println(kvp[0] + "\t" + kvp[1]);
			if (kvp[0].equals("device.device")){
				m_kvps.put("pchostname", kvp[1]);
			}
			else if (kvp[0].equals("agent.agent")){
				m_kvps.put("ntlogin", kvp[1]);
				System.out.println(kvp[1]);
			}
			else {
				m_kvps.put(kvp[0], kvp[1]);
			}
		}
		return m_kvps.get("method");
	}
}
