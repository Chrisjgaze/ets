package com.verint.services.adapters.eQC;

import java.util.Date;
import java.util.Map;

import com.verint.services.adapters.Genesys.DNStatus;
import com.verint.services.adapters.Genesys.GenesysManager;
import com.verint.services.model.Line;

public class Dashboard {

	public Dashboard() {

	}
	public String Response(){
		
		String s_response = "<html>\n"
			+ "<basefont face = \"Arial\">"
			+ "<meta http-equiv=\"refresh\"content=\"60\">"
			+ "<head><title>Verint Systems</title></head>\n"
		    + "<body bgcolor=\"white\">\n"
		    + "<h1>Extension Status - "
		    + new Date(System.currentTimeMillis()).toString()
		    + "</h1>\n"
		    + "<table border= 2px cellpadding='5' cellspacing='0' align ='center' "
		    + "style='border: solid 2px #4F81BD; font-size: small;'>"
		    + "<tr align='left' valign='top' bgcolor=#58ACFA>"
		    + "<td align='left' valign='top'>Extension</td>"
		    + "<td align='left' valign='top'>Status</td>"
		    + "<td align='left' valign='top'>Last Seen</td>"
		    + "<td align='left' valign='top'>Last Event</td>"
		    + "<td align='left' valign='top'>Adapter</td>"
		    + "</tr>";
		s_response = s_response + GenerateTable();
		s_response = s_response 
			+ "</table>"
		    + "</body></html>\n";                                       
		return s_response;

	}
	private String GenerateTable() {
		try {

		String s_ret = "";
		String s_extn;
		DNStatus status;
		Date d_now = new Date(System.currentTimeMillis());
		//List<Recording> m_list = NotificationServices.m_ct.getAll();
		Map<String, DNStatus> m_status = Line.getStatus();
		s_ret = "<tr align='left' valign='top'>Total Extensions:" +m_status.size() +"</td>";
		for (String s: m_status.keySet()) {
			status = m_status.get(s);
			s_ret = s_ret 
				+"<tr align='left' valign='top'>"
				+ "<td align='left' valign='top'>"
				+ s.substring(0,(s.indexOf(":")))
				+ "</td>"
				+ "<td align='left' valign='top'>"
				+ status.getDNStatus()
				+ "</td>"
				+ "<td align='left' valign='top'>"
				+ status.getLastSeen()
				+ "</td>"
				+ "<td align='left' valign='top'>"
				+ status.getLastEvent()
				+ "</td>"
				+ "<td align='left' valign='top'>"
				+ status.getAdapter()
				+ "</td>";
		}
		return s_ret;
		}
		catch (Exception e) {
			System.out.println("Exception generating html:" +e.getMessage());
			String s_ret = "<tr align='left' valign='top'>"
				+ "<td align='left' valign='top'></td>"
				+ "<td align='left' valign='top'></td>"
				+ "<td align='left' valign='top'></td>"
				+ "<td align='left' valign='top'></td>";
			return s_ret;
		}
	}
}
