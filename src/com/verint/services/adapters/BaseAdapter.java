package com.verint.services.adapters;

import com.witness.common.CCL.CommonCryptoLibrary;

public class BaseAdapter {
	
	public BaseAdapter() {
		
	}
    public String getDecryptedString(String name, String def) {
        String plaintext = getString(name, null);
        if (null == plaintext) return def;
        if (plaintext.length() > 0) {
          return CommonCryptoLibrary.decryptString(getString(name, def), CommonCryptoLibrary.CryptoAlgorithm.CCL_ALGORITHM_AES, CommonCryptoLibrary.StringEncoding.CCL_ENCODING_HEX);
        }

        return plaintext;
	}
    public String getString(String name) { return getString(name, ""); } 
    public String getString(String name, String def) {
      String value = def;
      String data = null;

      data = (String)name.toLowerCase();
      if (data != null) {
        value = data;
      }
      return value;
    }

}
