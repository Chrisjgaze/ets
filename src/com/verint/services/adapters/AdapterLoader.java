package com.verint.services.adapters;

import java.util.Map;
import com.verint.services.adapters.Genesys.GenesysManager;

public class AdapterLoader {
	
	private GenesysManager gManager;

	public AdapterLoader() { }
	public boolean addAdapter(String type, Map<String, String> adapterAttributes) {
		if (type.equalsIgnoreCase("GenesysSDK")) {
			if (null == gManager) {
				gManager = new GenesysManager();
			}
			gManager.addAdapter(adapterAttributes);
		}
		return true;
	}
}
