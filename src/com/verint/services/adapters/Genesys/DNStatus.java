package com.verint.services.adapters.Genesys;

import com.witness.common.wdls.WitLogger;


public class DNStatus {
	private final WitLogger m_log = WitLogger.create();
	private String DNStatus;
	private String LastEvent;
	private String Adapter;
	private Long LastSeen;
	public boolean isRegistered;
	
	public DNStatus(String s_Status, String s_Event, String s_Adapter, Long d_last, Boolean isR) {
		this.DNStatus = s_Status;
		this.LastEvent = s_Event;
		this.Adapter = s_Adapter;
		this.LastSeen = d_last;
		this.isRegistered = isR;
	}
	public void isRegistered(boolean isR) {
		this.isRegistered = isR;
	}
	public String getStatus() {
		return this.DNStatus;
	}
	public String getAdapter() {
		return this.Adapter;
	}
	public String getDNStatus() {
		return this.DNStatus;
	}
	public Long getLastSeen() {
		return LastSeen;
	}
	public String getLastEvent(){
		return LastEvent;
	}
	public void print(){
		m_log.debug(this.DNStatus + "::" +this.LastEvent +"::" +this.isRegistered);
	}
}
