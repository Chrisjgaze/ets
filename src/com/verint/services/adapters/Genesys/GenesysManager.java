package com.verint.services.adapters.Genesys;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.verint.services.utilites.DataSourceParser;

public class GenesysManager {
	private static final Map<Integer, GenesysAdapter> m_adapters = new HashMap<Integer, GenesysAdapter>();
	private static GenesysManager m_GenesysManager = null;
	private DataSourceParser m_dsp = new DataSourceParser();

	public GenesysManager() {
	}

	public boolean addAdapter(Map<String, String> m_attr) {
		final String m_host;
		final String m_stbyhost;
		final String m_name;
		final int m_id;
		final int m_port;
		final int m_ds;
		final String filename;
		List<String> m_extns;

		m_id = Integer.parseInt(m_attr.get("a_id"));
		m_host = m_attr.get("ServerName");
		m_stbyhost = m_attr.get("FailoverServerName");
		m_name = m_attr.get("a_name");
		m_port = Integer.parseInt(m_attr.get("ServerPort"));
		m_ds = Integer.parseInt(m_attr.get("a_ds"));
		filename = "DataSource-" + m_ds + ".xml";

		GenesysAdapter genadapter = new GenesysAdapter(m_host, m_port, m_stbyhost, m_ds, m_id, m_name, filename);
		return true;
	}
	public GenesysAdapter getAdapter(int id) {
		GenesysAdapter genadapter = m_adapters.get(id);
		return genadapter;
	}

}