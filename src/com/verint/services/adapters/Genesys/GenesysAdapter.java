package com.verint.services.adapters.Genesys;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.genesyslab.platform.commons.collections.KVList;
import com.genesyslab.platform.commons.connection.configuration.PropertyConfiguration;
import com.genesyslab.platform.commons.connection.interceptor.AddpInterceptor;
import com.genesyslab.platform.commons.protocol.Endpoint;
import com.genesyslab.platform.commons.protocol.Message;
import com.genesyslab.platform.commons.protocol.ProtocolException;
import com.genesyslab.platform.voice.protocol.TServerProtocol;
import com.genesyslab.platform.voice.protocol.tserver.AddressType;
import com.genesyslab.platform.voice.protocol.tserver.ControlMode;
import com.genesyslab.platform.voice.protocol.tserver.RegisterMode;
import com.genesyslab.platform.voice.protocol.tserver.events.EventAgentLogin;
import com.genesyslab.platform.voice.protocol.tserver.events.EventAgentLogout;
import com.genesyslab.platform.voice.protocol.tserver.events.EventAgentNotReady;
import com.genesyslab.platform.voice.protocol.tserver.events.EventAgentReady;
import com.genesyslab.platform.voice.protocol.tserver.events.EventAnswerAccessNumber;
import com.genesyslab.platform.voice.protocol.tserver.events.EventAttachedDataChanged;
import com.genesyslab.platform.voice.protocol.tserver.events.EventDNOutOfService;
import com.genesyslab.platform.voice.protocol.tserver.events.EventDialing;
import com.genesyslab.platform.voice.protocol.tserver.events.EventError;
import com.genesyslab.platform.voice.protocol.tserver.events.EventEstablished;
import com.genesyslab.platform.voice.protocol.tserver.events.EventHeld;
import com.genesyslab.platform.voice.protocol.tserver.events.EventLinkConnected;
import com.genesyslab.platform.voice.protocol.tserver.events.EventLinkDisconnected;
import com.genesyslab.platform.voice.protocol.tserver.events.EventNetworkReached;
import com.genesyslab.platform.voice.protocol.tserver.events.EventOffHook;
import com.genesyslab.platform.voice.protocol.tserver.events.EventOnHook;
import com.genesyslab.platform.voice.protocol.tserver.events.EventPrimaryChanged;
import com.genesyslab.platform.voice.protocol.tserver.events.EventRegistered;
import com.genesyslab.platform.voice.protocol.tserver.events.EventReleased;
import com.genesyslab.platform.voice.protocol.tserver.events.EventRetrieved;
import com.genesyslab.platform.voice.protocol.tserver.events.EventRinging;
import com.genesyslab.platform.voice.protocol.tserver.events.EventServerInfo;
import com.genesyslab.platform.voice.protocol.tserver.events.EventUserEvent;
import com.genesyslab.platform.voice.protocol.tserver.requests.dn.RequestRegisterAddress;
import com.genesyslab.platform.voice.protocol.tserver.requests.queries.RequestQueryServer;
import com.verint.services.ExtensionTaggingService.ExtensionTaggingService;
import com.verint.services.alarms.TrapSender;
import com.verint.services.model.GenesysCall;
import com.verint.services.model.Line;
import com.verint.services.utilites.DataSourceParser;
import com.verint.services.utilites.FileWatcher;
import com.witness.common.threading.GlobalTimerWheel;
import com.witness.common.threading.TimerWheelCommand;
import com.witness.common.util.ShallowMap;
import com.witness.common.wdls.WitLogger;

public class GenesysAdapter implements FileWatcher.IFileWatcherCallBack {

	private final WitLogger m_log = WitLogger.create("GenesysAdp");
	private String m_name;
	private String m_TServerHost = "10.56.7.48";
	private String m_TServerStbyHost = "10.56.7.48";
	private int m_TServerPort = 7002;
	private TServerProtocol m_protocol;
	private boolean isConnected;
	private boolean m_onPrimary;
	private boolean canRegister;
	private boolean errorState;
	private int m_ds;
	private Map<String, DNStatus> m_extns = new HashMap<String, DNStatus>();
	private Map<Integer, String> m_requestTypeMap = new ShallowMap<Integer, String>();
	private DataSourceParser dsp = new DataSourceParser();
	private long m_lastevent = System.currentTimeMillis();
	private TimerWheelCommand m_command;

	public GenesysAdapter(String host, int port, String stbyhost, int ds, int id, String m_name, String s_file) {
		this.m_name = m_name;
		this.m_TServerHost = host;
		this.m_TServerPort = port;
		this.m_TServerStbyHost = stbyhost;
		this.m_ds = ds;
		if (m_name.contains("ICM")) {
			try {
				m_log.debug("<%s> Sleeping ICM Thread", m_name);
				Thread.sleep(10000);
			} catch (Exception e) {
				m_log.error("<%s> Exception sleeping thread, %s", e.getMessage());
			}

		}
		m_log.info("Creating new Genesys Adapter'%s', ID:%s, for DS:%s", m_name, id, ds);
		load(s_file);

		this.m_command = GlobalTimerWheel.getTimerWheel().submitRepeatedCommand(new CheckGenConn(), 60000L, 60000L);
		try {
			String s_body = String.format("Info - GenesysAdapter <%s> starting", m_name);
			// TrapSender.sendMail(s_body, "");
		} catch (Exception e) {
			m_log.error("Exception sending email, %s", e.getMessage());
		}
	}

	protected void sendHeartbeatRequest() {
		try {
			m_protocol.send(RequestQueryServer.create());
			m_log.debugHigh("<%s> Heartbeat sent", m_name);
		} catch (Exception e) {
			m_log.error("<%s> Exception in heartbeat, %s", m_name, e.getMessage());
			isConnected = false;
			clearExtns();
			disconnect();
			connect();
		}
	}

	private void clearExtns() {

	}

	private void readExtens(String calledFrom) {
		try {
			m_log.debugHigh("Adapter <%s> called from<%s>", this.m_name, calledFrom);
			List<String> extns = dsp.parse(m_ds);
			boolean needToregister = false;
			for (String s_extn : extns) {
				if (m_extns.containsKey(s_extn)) {
					m_log.debugHigh("Exsisting Extension<%s> found in<%s>", s_extn, this.m_name);
				} else {
					m_log.info("Adding new Extension<%s> to<%s>", this.m_name, s_extn);
					Long l_date = System.currentTimeMillis();
					DNStatus status = new DNStatus("Unknown", "", this.m_name, l_date, false);
					Line.addLine(s_extn, status);
					m_extns.put(s_extn, status);
					needToregister = true;
				}
			}
			m_log.debugHigh("<%s> Finished read", m_name);
			if (needToregister && isConnected) {

			}

		} catch (Exception e) {
			m_log.error("<%s> Exception in readExtns:%s", m_name, e.getMessage());
		}
	}

	public void load(String s_file) {
		try {
			FileWatcher.addFileToWatch(s_file, this);
			readExtens("Constructor");
			connect();
			registerExtensions();
			Runnable treatEvent = new Thread() {
				public void run() {
					try {
						runThreadFunction();

					} catch (Exception e) {
						m_log.error("<%s> Exception in EventRegistered: %s", m_name, e.getMessage());
					}
				}
			};
			Thread doEvent = new Thread(treatEvent);
			doEvent.start();

			// m_log.debug("<%s> Finished load", this.m_name);
		} catch (Exception e) {
			m_log.error("<%s> Exception loading adapter %s", m_name, e.getMessage());
		}
	}
	private String getHost() {
		return (m_onPrimary ? m_TServerHost : m_TServerStbyHost);
	}
	private int getPort() {
		return (m_onPrimary ? m_TServerPort : m_TServerPort);
	}

	public void connect() {
		m_onPrimary = true;
		m_log.debug("<%s> Connecting to TServer:%s", m_name, m_TServerHost);
		if (tryConnect()) {
			m_log.debug("no exception reported");
			return;
		} else {
			m_log.debug("<%s> Exception connecting to primary, try secondary", m_name);
			tryConnect();
			m_onPrimary = false;
		}

	}
	private boolean tryConnect() {
		try {
			String host = getHost();
			int port = getPort();
			m_log.info("<%s> Connecting to:%s on port: %s", m_name, host, port);
			m_protocol = null;
			m_protocol = new TServerProtocol(new Endpoint(host, host, port));
			PropertyConfiguration connectionConf = new PropertyConfiguration();
			connectionConf.setOption(AddpInterceptor.PROTOCOL_NAME_KEY, "addp");
			connectionConf.setOption(AddpInterceptor.REMOTE_TIMEOUT_KEY, "60");
			connectionConf.setOption(AddpInterceptor.TIMEOUT_KEY, "30");
			connectionConf.setOption(AddpInterceptor.TRACE_KEY, "client");
			m_protocol.configure(connectionConf);
			m_protocol.open();
			isConnected = true;
			return true;
		} catch (Exception e) {
			m_log.error("<%s> Exception connecting to T-Server: %s", m_name, e.getMessage());
			isConnected = false;
			return false;
		}
	}
	public void disconnect() {
		try {
			if (null == m_protocol)
				return;
			m_protocol.close();
			isConnected = false;
		} catch (Exception e) {
			m_log.error("<%s> Exception in disconnecting", m_name);
		}
	}

	public void registerExtensions() {

		m_log.debugHigh("Register Extensions");
		Runnable t_AP = new Thread() {
			public void run() {
				try {
					canRegister = true;
					int i = 0;
					mainloop : for (Map.Entry<String, DNStatus> dn : m_extns.entrySet()) {
						if (!canRegister) {
							m_log.debugHigh("Detected connection to T-Server has dropped");
							break mainloop;
						}
						DNStatus status = dn.getValue();
						// m_log.debugHigh("Got status <%s>", status.print());
						m_log.debugHigh("<%s> Process Extension<%s> with state<%s>", m_name, dn.getKey(), status.getStatus());
						if (status.isRegistered) {

						} else {

						}
						m_log.debugHigh("Request to register Extension <%s> request number:%s", dn.getKey(), i);
						try {
							registerDN(dn.getKey());
							Thread.sleep(200);
							i++;
						} catch (Exception e) {
							m_log.debugHigh("regExtns err:%s", e.getMessage());
						}

					}
				} catch (Exception e) {
					m_log.error("<%s> Exception Registering extensions %s", m_name, e.getMessage());
				}
			}
		};
		Thread run_AP = new Thread(t_AP);
		run_AP.start();
	}

	protected void handleRegistered(final Message msg) {

		try {
			String s_dn = (String) msg.getMessageAttribute("ThisDN");
			long l_date = System.currentTimeMillis();
			DNStatus status = new DNStatus("Registered", "Registered", m_name, l_date, true);
			m_log.info("<%s> EventRegistered for DN:%s", m_name, s_dn);
			status.print();
			Line.addLine(s_dn, status);
			m_extns.put(s_dn, status);

		} catch (Exception e) {
			m_log.error("<%s> Exception in EventRegistered: %s", m_name, e.getMessage());
		}

	}

	protected void handleEventEstablished(final Message msg) {
		try {
			Map<String, String> m_attrs = new HashMap<String, String>();
			String s_sipID = "";
			String s_uuid = "";
			String s_pai = "";
			String s_dn = "";
			String s_ani = null;
			String s_dnis = null;
			try {
				s_dn = (String) msg.getMessageAttribute("ThisDN");
				s_uuid = (String) msg.getMessageAttribute("CallUuid");
			} catch (Exception e) {
				m_log.error("Exception getting DN / UUID detail %s", e.getMessage());
			}

			try {
				EventEstablished msga = (EventEstablished) msg;
				s_ani = msga.getANI();
				if (s_ani == null) {
					s_ani = "";
				}
				s_dnis = msga.getDNIS();
				if (s_dnis == null) {
					s_dnis = "";
				}
			} catch (Exception eMsg) {
				m_log.error("Error converting message:%s", eMsg.getMessage());
			}
			try {
				KVList m_kvl = (KVList) msg.getMessageAttribute("UserData");
				if (m_kvl.containsKey("Call-ID")) {
					s_sipID = m_kvl.getString("Call-ID");
					s_sipID = "SIP/" + s_sipID;
				}
				m_attrs.put("sipid", s_sipID);
				// P-Asserted-Identity
				if (m_kvl.containsKey("P-Asserted-Identity")) {
					s_pai = m_kvl.getString("P-Asserted-Identity");
				}
			} catch (Exception e) {
				m_log.error("<%s> Exception getting KVP detail %s", m_name, e.getMessage());
			}
			m_attrs.put("sipid", s_sipID);
			m_attrs.put("pai", s_pai);
			m_attrs.put("dn", s_dn);
			m_attrs.put("uuid", s_uuid);
			m_attrs.put("ani", s_ani);
			m_attrs.put("dnis", s_dnis);
			m_log.info("<%s> EventEstablished on DN: %s ; ANI:%s ; DNIS:%s ; SIPID:%s", m_name, s_dn, s_ani, s_dnis, s_sipID);
			Long l_time = System.currentTimeMillis();
			DNStatus status = new DNStatus("Registered", "EventEstablished", m_name, l_time, false);
			Line.addLine(s_dn, status);
			// m_log.debugHigh("EventEstablished for SIP ID:%s with uuid: %s on DN:%s",s_sipID,
			// s_uuid, s_dn);
			GenesysCall m_gcall = new GenesysCall(m_attrs);
			ExtensionTaggingService.m_gct.addCall(m_gcall, m_log);
		} catch (Exception e) {
			m_log.error("<%s> Exception in EventEstablished: %s", m_name, e.getMessage());
		}

	}
	protected void handleEventReleased(final Message msg) {
		try {
			Map<String, String> m_attrs = new HashMap<String, String>();
			String s_sipID = "";
			String s_uuid = "";
			String s_pai = "";
			String s_ani = null;
			String s_dnis = null;
			String s_dn = "";
			try {
				s_dn = (String) msg.getMessageAttribute("ThisDN");
				s_uuid = (String) msg.getMessageAttribute("CallUuid");
			} catch (Exception e) {
				m_log.error("Exception getting DN / UUID detail %s", e.getMessage());
			}
			try {
				KVList m_kvl = (KVList) msg.getMessageAttribute("UserData");
				if (m_kvl.containsKey("Call-ID")) {
					s_sipID = m_kvl.getString("Call-ID");
					s_sipID = "SIP/" + s_sipID;
				}
				if (s_sipID.length() <= 0) {
					s_sipID = s_uuid;
				}
				if (m_kvl.containsKey("P-Asserted-Identity")) {
					s_pai = m_kvl.getString("P-Asserted-Identity");
				}

			} catch (Exception e) {
				// m_log.error("Exception getting KVP detail %s",
				// e.getMessage());
			}

			m_attrs.put("sipid", s_sipID);
			try {
				EventReleased msga = (EventReleased) msg;
				s_ani = msga.getANI();
				if (s_ani == null) {
					s_ani = "";
				}
				s_dnis = msga.getDNIS();
				if (s_dnis == null) {
					s_dnis = "";
				}
			} catch (Exception eMsg) {
				m_log.error("Error converting message:%s", eMsg.getMessage());
			}
			m_log.info("<%s> EventReleased on DN: %s ; ANI:%s ; DNIS:%s", m_name, s_dn, s_ani, s_dnis);
			Long l_time = System.currentTimeMillis();
			DNStatus status = new DNStatus("Registered", "EventReleased", m_name, l_time, false);
			Line.addLine(s_dn, status);
			m_attrs.put("sipid", s_sipID);
			m_attrs.put("pai", s_pai);
			m_attrs.put("dn", s_dn);
			m_attrs.put("uuid", s_uuid);
			m_attrs.put("ani", s_ani);
			m_attrs.put("dnis", s_dnis);
			GenesysCall m_gcall = new GenesysCall(m_attrs);
			ExtensionTaggingService.m_gct.removeCall(m_gcall, m_log);
		} catch (Exception e) {
			m_log.error("<%s> Exception in EventReleased: %s", m_name, e.getMessage());
		}
	}
	protected void handleEventRetrieved(final Message msg) {
		try {
			Map<String, String> m_attrs = new HashMap<String, String>();
			String s_sipID = "";
			String s_uuid = "";
			String s_pai = "";
			String s_dn = (String) msg.getMessageAttribute("ThisDN");
			String s_ani = null;
			String s_dnis = null;
			try {
				EventRetrieved msga = (EventRetrieved) msg;
				s_ani = msga.getANI();
				if (s_ani == null) {
					s_ani = "";
				}
				s_dnis = msga.getDNIS();
				if (s_dnis == null) {
					s_dnis = "";
				}
			} catch (Exception eMsg) {
				m_log.error("Error converting message:%s", eMsg.getMessage());
			}
			m_log.info("<%s> EventRetrieved on DN: %s ; ANI:%s ; DNIS:%s", m_name, s_dn, s_ani, s_dnis);
			s_uuid = (String) msg.getMessageAttribute("CallUuid");
			KVList m_kvl = (KVList) msg.getMessageAttribute("UserData");
			if (m_kvl.containsKey("Call-ID")) {
				s_sipID = m_kvl.getString("Call-ID");
				s_sipID = "SIP/" + s_sipID;

				// UR_RECORD
			}
			m_attrs.put("sipid", s_sipID);
			// P-Asserted-Identity
			if (m_kvl.containsKey("P-Asserted-Identity")) {
				s_pai = m_kvl.getString("P-Asserted-Identity");
			}
			m_attrs.put("sipid", s_sipID);
			m_attrs.put("pai", s_pai);
			m_attrs.put("dn", s_dn);
			m_attrs.put("uuid", s_uuid);
			m_attrs.put("ani", s_ani);
			m_attrs.put("dnis", s_dnis);
			m_log.debugHigh("<%s> EventRetrieved for SIP ID:%s with uuid: %s on DN:%s", m_name, s_sipID, s_uuid, s_dn);
			GenesysCall m_gcall = new GenesysCall(m_attrs);
			ExtensionTaggingService.m_gct.addCall(m_gcall, m_log);
		} catch (Exception e) {
			m_log.error("<%s> Exception in EventRetrieved: %s", m_name, e.getMessage());
		}
	}

	protected void handleOutOfService(final Message msg) {

		try {
			String s_DN = (String) msg.getMessageAttribute("ThisDN");
			Long l_time = System.currentTimeMillis();
			m_log.debugHigh("<%s> Out of Service received for Extn<%s>", m_name, s_DN);
			DNStatus status = new DNStatus("UnRegistered", "OutOfService", m_name, l_time, false);
			Line.addLine(s_DN, status);
			m_extns.put(s_DN, status);
			registerDN(s_DN);
		} catch (Exception e) {
			m_log.error("<%s> Exception in EventOutOfService: %s", m_name, e.getMessage());
		}

	}

	protected void handleEventError(final Message msg) {

		try {
			EventError m_err = (EventError) msg;
			m_log.debugHigh("<%s> EventError received from T-Server for DN:%s Error:%s", m_name, m_err.getThisDN(), m_err.getErrorMessage());

		} catch (Exception e) {
			m_log.error("<%s> Exception in EventError: %s", m_name, e.getMessage());
		}

	}

	protected void handleEventDisconnected(final Message msg) {

		try {
			m_log.debugHigh("<%s> EventDisconnected received from T-Server", m_name);

		} catch (Exception e) {
			m_log.error("<%s> Exception in EventOutOfService: %s", m_name, e.getMessage());
		}

	}

	protected void handlePrimaryChanged(final Message msg) {

		try {
			m_log.debugHigh("Primary T-Server has changed");

		} catch (Exception e) {
			m_log.error("<%s> Exception in EventOutOfService: %s", m_name, e.getMessage());
		}

	}

	protected void handleEventServerInfo(final Message msg) {

		try {
			m_log.debugHigh("<%s> EventServerInfo:%s", m_name, msg.toString());

		} catch (Exception e) {
			m_log.error("<%s> Exception in EventOutOfService: %s", m_name, e.getMessage());
		}

	}

	protected void registerDN(String dn) throws Exception {
		RequestRegisterAddress msg = RequestRegisterAddress.create(dn, RegisterMode.ModeShare, ControlMode.RegisterDefault, AddressType.DN);
		sendMessage(msg, "RequestRegister");
	}
	private boolean sendMessage(Message msg, String msgName) throws Exception {
		Integer refId = null;
		try {
			// synchronized (this.m_requestTypeMap) {
			this.m_protocol.send(msg);

			// refId = (Integer) msg.getMessageAttribute("ReferenceID");
			// this.m_requestTypeMap.put(refId, msgName);
			// } n
		} catch (ProtocolException e) {
			m_log.error("Error sending Request<%s> RefId<%d>.  <%s>", e, new Object[]{msgName, refId, e});
			return false;
		}
		return true;
	}
	protected void runThreadFunction() throws Exception {
		while (isConnected) {
			Message evnt = m_protocol.receive(500L);
			if (null != evnt) {
				m_lastevent = System.currentTimeMillis();
				switch (evnt.messageId()) {
					case EventLinkConnected.ID :
						m_log.debugHigh("<%s> EventLinkConnected received", m_name);
						registerExtensions();
						break;
					case EventRegistered.ID :
						handleRegistered(evnt);
						break;
					case EventEstablished.ID :
						handleEventEstablished(evnt);
						break;
					case EventReleased.ID :
						handleEventReleased(evnt);
						break;
					case EventRetrieved.ID :
						handleEventRetrieved(evnt);
						break;
					case EventDNOutOfService.ID :
						handleOutOfService(evnt);
						break;
					case EventError.ID :
						handleEventError(evnt);
						break;
					case EventLinkDisconnected.ID :
						handleEventDisconnected(evnt);
						break;
					case EventPrimaryChanged.ID :
						handlePrimaryChanged(evnt);
						break;
					case EventServerInfo.ID :
						handleEventServerInfo(evnt);
						break;
					case EventAttachedDataChanged.ID :
						break;
					case EventRinging.ID :
						break;
					case EventDialing.ID :
						break;
					case EventHeld.ID :
						break;
					case EventAgentReady.ID :
						break;
					case EventAgentNotReady.ID :
						break;
					case EventOffHook.ID :
						break;
					case EventOnHook.ID :
						break;
					case EventNetworkReached.ID :
						break;
					case EventUserEvent.ID :
						break;
					case EventAgentLogin.ID :
						break;
					case EventAgentLogout.ID :
						break;
					default :
						m_log.debugHigh("<%s> Unused message:%s", m_name, evnt.messageName());
						break;
				}
			}
		}
	}
	public void fileModified() {
		m_log.debugHigh("Notification of update to datasource");
		readExtens("FileUpdate");
	}

	private class CheckGenConn implements Runnable {
		private CheckGenConn() {
		}
		public void run() {
			m_log.info("<%s> Check connection to Genesys", GenesysAdapter.this.m_name);
			sendHeartbeatRequest();
			if (m_lastevent - System.currentTimeMillis() > 60000) {
				if (!errorState) {
					String s_body = String.format("Error - GenesysAdapter <%s> reporting dead", m_name);
					// TrapSender.sendMail(s_body, "Attempting to restart.");
				}
				m_log.error("<%s> Connection to Genesys reports dead, reset", m_name);
				errorState = true;
				try {
					disconnect();
					connect();
					runThreadFunction();
				} catch (Exception e) {
					m_log.error("<%s> Could not reconnect to Genesys", m_name);
				}
			} else {
				if (errorState) {
					String s_body = String.format("Info - GenesysAdapter <%s> seen events", m_name);
					// TrapSender.sendMail(s_body, "");
					errorState = false;
				}
			}

		}
	}
}
