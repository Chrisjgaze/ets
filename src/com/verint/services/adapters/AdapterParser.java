package com.verint.services.adapters;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.verint.services.utilites.ConfigurationUtility;
import com.witness.common.util.XMLHelper;

public class AdapterParser{
	private boolean m_isServerXml = false;
	AdapterLoader adapterLoader = new AdapterLoader();
	public AdapterParser(){
		
		start();
	}
	public void start(){
		String fileName = "IntegrationService.xml";
	    File file = new File(fileName);
	    Document doc = XMLHelper.readXMLFile(fileName, ConfigurationUtility.getConfigurationFile(fileName));
	    //Document doc = XMLHelper.readXMLFile(filename, file);
	    if (null == doc) {
	      return;
	    }
        Element root = doc.getDocumentElement();
        Element integrations = XMLHelper.getElementByName(root, "Integrations");
        for (Element integration : XMLHelper.getElementsByName(integrations, "Integration")) {
            handleAdapterConfig(integration, false);
          }
	}
	private void handleAdapterConfig(Element integration, Boolean isUpdate) {
		Map<String, String> adapterAttributes = new HashMap();
		int id = XMLHelper.getAttributeInt(integration, "Identifier");
		adapterAttributes.put("a_id", Integer.toString(id));
		int ds = XMLHelper.getAttributeInt(integration, "DataSourceIdentifier");
		adapterAttributes.put("a_ds", Integer.toString(ds));
		String name = integration.getAttribute("Name");
		adapterAttributes.put("a_name", name);
		String type = integration.getAttribute("Type");
		adapterAttributes.put("a_type", type);
	    for (Element set : XMLHelper.getElementsByName(integration, "Set")) {
	    	adapterAttributes.put(set.getAttribute("Key"), set.getAttribute("Value"));
	        //System.out.println(set.getAttribute("Key"));
	        //System.out.println(set.getAttribute("Value"));
	        //adapter.setProperty(key, value);
	      }
		adapterLoader.addAdapter(type, adapterAttributes);
	}


}
