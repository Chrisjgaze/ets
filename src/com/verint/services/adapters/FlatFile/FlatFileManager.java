package com.verint.services.adapters.FlatFile;

import java.util.HashMap;
import java.util.Map;

public class FlatFileManager {
	Map<Integer, FlatFileAdapter> m_adapters = new HashMap();
	
	public FlatFileManager() {	}
	public boolean addAdapter(int id, String name, Map<String, String> adapterAttributes) {
		FlatFileAdapter ffadapter = m_adapters.get(id);
		if (null == ffadapter)
			ffadapter = new FlatFileAdapter();
		ffadapter.setAttributes(adapterAttributes);
		m_adapters.remove(id);
		m_adapters.put(id, ffadapter);
		return true;
	}
	public FlatFileAdapter getAdapter(int id) {
		FlatFileAdapter ffadapter = m_adapters.get(id);
		return ffadapter;
	}
}