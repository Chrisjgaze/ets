package com.verint.services.adapters.FlatFile;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import com.verint.services.utilites.FileWatcher;
import com.witness.common.wdls.WitLogger;

public class FlatFileAdapter implements FileWatcher.IFileWatcherCallBack {
	private final WitLogger m_log = WitLogger.create("FlatFileAd");
	private static Map<String, String> m_extensions = new HashMap();
	static String m_file;
	
	public FlatFileAdapter(){};
	
	public boolean setAttributes(Map<String, String> adapterAttributes){
		m_log.debug("Setting Attributes for flat file adapter");
		for (Map.Entry entry : adapterAttributes.entrySet()){
			addAttribute((String)entry.getKey(), (String)entry.getValue());
			m_log.debug("Adapter attribute:%s value:%s",(String)entry.getKey(), (String)entry.getValue());
		}
		return true;
	}
	private void addAttribute(String name, String value) {
		if (name.equalsIgnoreCase("FilePath")){
			m_log.debug("FilePath found");
			this.m_file = value;
		}
		m_log.debug("Configured file:%s", this.m_file);
		start();
	}
	public boolean start(){
		FileWatcher.addFileToWatch(this.m_file, this);
		try {
			readFile();
		}catch (FileNotFoundException fnfe) {
			
		}catch (IOException ioe){
			
		}
		return true;
	}
	public void fileModified() {
		m_log.debug("Adapter notified of change to file");
		m_extensions.clear();
		try {
			readFile();
		} catch (FileNotFoundException e) {
			m_log.debug("Error caught reading file %s",e.getMessage());
		} catch (IOException e) {
			m_log.debug("Error caught reading file %s",e.getMessage());
		}
	}
	public void readFile() throws FileNotFoundException, IOException{
		File file = new File(m_file);
		BufferedReader bufRdr  = new BufferedReader(new FileReader(file));
		String line = null;
		while((line = bufRdr.readLine()) != null) {
			StringTokenizer st = new StringTokenizer(line,"@");
				try{
					String s_ext = st.nextToken().toString().trim();
					String s_ip = st.nextToken().toString().trim();
					m_log.debug("Adding ip %s with extension %s to memeory", s_ip, s_ext);
					m_extensions.put(s_ip, s_ext);
				}
				catch(java.util.NoSuchElementException nsee){
				}
		}
		bufRdr.close();
		return;
	}
	public String getExtension(String s_ip){
		if (!(null == m_extensions.get(s_ip))) {
			m_log.debug("Matched ip %s to extension %s");
			return m_extensions.get(s_ip);
		}
		return "";
	}
}
