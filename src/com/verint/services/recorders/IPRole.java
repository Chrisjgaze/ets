package com.verint.services.recorders;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.verint.services.ExtensionTaggingService.CheckCallHistory;
import com.verint.services.ExtensionTaggingService.ExtensionTaggingService;
//import com.verint.services.adapters.eQC.EQCServer;
import com.verint.services.model.BlockedCall;
import com.verint.services.model.BlockedCallTracker;
import com.verint.services.model.CallTracker;
import com.verint.services.model.Recording;
import com.verint.services.utilites.DeleteInum;
import com.witness.common.NGA.IComponentRegistration;
import com.witness.common.NGA.IProcessNGAMessage;
import com.witness.common.NGA.NGAConnection;
import com.witness.common.NGA.NGAClient;
import com.witness.common.NGA.NGAMessage;
import com.witness.common.NGA.NGAConnection.ReasonLost;
import com.witness.common.threading.GlobalTimerWheel;
import com.witness.common.threading.TimerWheelCommand;
import com.witness.common.util.HexUtils;
import com.witness.common.wdls.WitLogger;

public class IPRole implements IComponentRegistration, IProcessNGAMessage {
	private final WitLogger m_log = WitLogger.create("IPRole");
	private String m_hostname;
	private int m_id;
	private int m_port = 29504;
	protected NGAConnection m_ngaConnection;
	protected NGAClient client = null;
	protected TimerWheelCommand m_command;
	protected Object m_commandLock = new Object();
	private int m_commandId = 1;
	private int m_update = 1;
	private Map<String, Recording> m_activeRecordings = new HashMap();
	private Map<String, String> m_attributes = new HashMap();
	private List<String> m_extn = new ArrayList<String>();
	private CheckCallHistory m_cch = new CheckCallHistory();
	private long m_lastSeen = System.currentTimeMillis();
	private boolean isDead = true;

	public IPRole(int id, String hostname) {
		m_log.debugHigh("Creating Role for recorder id:%s", hostname);
		this.m_id = id;
		this.m_hostname = hostname;
		start();
	}
	public boolean start() {
		long l_thread = Thread.currentThread().getId();
		this.m_log.info("Thread <" + l_thread + " > Get connection to Recorder %s on port %s", m_hostname, m_port);
		if (this.client == null) {
			this.client = new NGAClient(m_hostname, m_port, 1, "ETS", "ETSService", this);
			this.m_command = GlobalTimerWheel.getTimerWheel().submitRepeatedCommand(new CheckConnection(), 5000L, 5000L);
			return this.client.reset();
		}

		return true;
	}
	public void stop() {
		if (this.m_command != null)
			this.m_command.cancel();
		// this.m_command.cancelCommand();
		this.m_command = null;
		if (this.m_ngaConnection != null)
			this.m_ngaConnection.close();
		this.m_ngaConnection = null;
		this.client.Shutdown();
	}
	public void UnregisterComponent(NGAConnection connection, NGAConnection.ReasonLost reason) {
		this.m_log.debugHigh("Connection lost:%s", reason.toString());
		this.m_ngaConnection = null;
	}

	public void RegisterComponent(NGAConnection connection) {
		this.m_log.debug("Register listener:%s", connection.toString());
		this.m_ngaConnection = connection;
		connection.setCallback(this);
		connection.startPingRequest(10000);
	}
	public void checkForBackup(int arg0) {
	}

	public void ProcessNGAMessage(NGAMessage m_message) {
		this.m_log.dev("<%s>, process NGA message: %s", m_hostname, m_message.Command);
		isDead = false;
		String cmd = m_message.Command;
		m_lastSeen = System.currentTimeMillis();
		try {
			if ("TAGGED".equalsIgnoreCase(cmd))
				IPRole.this.processTagged(m_message);
			else if ("STARTED".equalsIgnoreCase(cmd))
				IPRole.this.processStarted(m_message);
			else if ("STOPPED".equalsIgnoreCase(cmd))
				IPRole.this.processStopped(m_message);
			else if ("HELLO".equalsIgnoreCase(cmd))
				IPRole.this.processHello(m_message);
			else if ("UPDATE".equalsIgnoreCase(cmd))
				IPRole.this.processUpdateMessage(m_message);
			else if ("EXTENDEDINFO_RESPONSE".equalsIgnoreCase(cmd))
				IPRole.this.processExtendedInfoResponse(m_message);
			else if ("FALLBACK".equalsIgnoreCase(cmd))
				IPRole.this.processFallback(m_message);
			else if ("STARTEXTENSIONUPDATE".equalsIgnoreCase(cmd))
				IPRole.this.processStartExtensionUpdate();
		} catch (Exception e) {
			m_log.error("<%s> error processing NGA Message:%s", e.getMessage());
		}
	}
	private void processStartExtensionUpdate() {
		// m_log.debug("Extension update message received, clearing list");
		// m_extn.clear();
	}
	private void processFallback(NGAMessage message) {
		// m_log.info("Fallback received");
	}
	private void processExtendedInfoResponse(NGAMessage m_message) {
		// m_log.debug("Received EXTENDEDINFO_RESPONSE");
	}
	private void processHello(NGAMessage message) {
		try {
			sendQuery();
		} catch (Exception e) {
			m_log.error("<%s> Error processing HELLO message: %s", this.m_hostname, e.getMessage());
		}
	}
	private void processStarted(NGAMessage message) {
		this.m_log.info("<%s> Process StartedMessage: %s", this.m_hostname, message.Inum);
		try {
			String s_DN = null;
			Recording recording = new Recording(message.Inum, m_hostname, "Audio");
			recording.m_role = this;
			m_attributes.clear();
			for (Map.Entry<String, String> entry : message.getParameters().entrySet()) {
				m_log.dev("pt:%s, val:%s", entry.getKey().toString(), entry.getValue().toString());
				m_attributes.put(entry.getKey().toString(), entry.getValue().toString());
			}
			// check for LASTINUM
			try {
				m_log.trace("Get last inum");
				String s_lastInum = m_attributes.get("LASTINUM");
				m_log.trace("Got last inum");
				s_lastInum = s_lastInum.replace("-", "");
				m_log.trace("LastInum found <%s>", s_lastInum);
				if (CallTracker.keepCall(s_lastInum)) {
					m_log.debugHigh("Found call with previous inum <%s> update keep flag", s_lastInum);
					sendKeepTrue(message.Inum);
					recording.m_keep = true;
				} else {
					m_log.debugHigh("No previous inum in tracker");
				}
			} catch (Exception e) {
				m_log.debugHigh("No previous Inum to trace");
			}

			String s_CallRef = m_attributes.get("CALLID");
			recording.m_sipid = s_CallRef;
			String s_uuid = m_attributes.get("X-GENESYS-CALLUUID");
			String s_pai = m_attributes.get("P-Asserted-Identity");
			String s_dnis = m_attributes.get("FROMNUMBER");
			String s_ani = m_attributes.get("TONUMBER");
			Map<String, String> m_srch = new HashMap<String, String>();
			m_srch.put("sipid", s_CallRef);
			m_srch.put("uuid", s_uuid);
			m_srch.put("ani", s_ani);
			m_srch.put("dnis", s_dnis);
			try {
				s_DN = ExtensionTaggingService.m_gct.getDN(m_srch, m_log);
				if (s_DN.equals("")) {
					m_log.dev("No DN found from tracker");
				}
				// m_log.debugHigh("Found DN:%s", s_DN);
			} catch (Exception e1) {
				m_log.error("Exception getting DN from tracker.");
			}
			if (s_DN.length() > 1) {
				m_log.dev("Tagging extension %s to callid %s", s_DN, s_CallRef);
				Map<String, String> tag = new HashMap<String, String>();
				tag.put("Extension", s_DN);
				sendIPTagMessage(message.Inum, tag);
			} else {
				m_log.debugHigh("No Genesys call to match for id:%s", s_CallRef);
			}
			if (CallTracker.checkForBlocks(s_CallRef)) {
				recording.m_isBlocked = true;
			}
			// sendKeepFalse(message.Inum);
			this.m_activeRecordings.put(message.Inum, recording);
			CallTracker.addCall(message.Inum, recording);
			this.m_log.debugHigh("<%s> Finished processing StartedMessage: %s", this.m_hostname, message.Inum);
		} catch (Exception e) {
			m_log.error("Error processing Started message: %s", e.getMessage());
		}
	}
	private void processStopped(NGAMessage message) {
		this.m_log.info("<%s> Process StoppedMessage: %s", this.m_hostname, message.Inum);
		try {
			this.m_activeRecordings.remove(message.Inum);
			CallTracker.removeCall(message.Inum);
			String callRef = message.getCallRef();
			this.m_log.info("<%s> STOPPED: CallRef %s", this.m_hostname, callRef);
		} catch (Exception e) {
			m_log.error("Error processing stopped message: %s", e.getMessage());
		}
	}
	private boolean processTagged(NGAMessage message) {
		this.m_log.dev("<%s> Process Tagged Message: %s", this.m_hostname, message.Inum);
		try {
			Recording recording = this.getRecording(message.Inum);
			if (null != recording) {
				for (Map.Entry<String, String> entry : message.getParameters().entrySet()) {
					// System.out.println("pt:" +entry.getKey().toString() +
					// " val:" +entry.getValue().toString());
					if (entry.getKey().toString().equalsIgnoreCase("EXTENDEDCALLHISTORY")) {
						String exHistory = entry.getValue().toString();
						m_log.dev("Got call %s with EXTENDEDCALLHISTORY: %s checking history", recording.m_inum, exHistory);
						if (exHistory.contains("BlockContact")) {
							recording.m_isBlocked = true;
							m_log.dev("Call was blocked, set blocked flag to true");
							String s_CallRef = recording.m_sipid;
							BlockedCall call = new BlockedCall();
							call.globalcallid = s_CallRef;
							call.insertedAt = new Date(System.currentTimeMillis());
							m_log.dev("Extended history shows call as blocked :%s adding to blocked call tracker.", s_CallRef);
							// ExtensionTaggingService.m_BCT.addCall(s_CallRef,
							// call);
							checkBlockedCall(s_CallRef);
							recording.m_isBlocked = true;
						}
					}
					if (entry.getKey().toString().equalsIgnoreCase("CD24")) {
						String s_cd24 = entry.getValue().toString();
						if (s_cd24.equalsIgnoreCase("9999") && !recording.m_isBlocked) {
							m_log.dev("CD24 length match: %s", message.Inum);
							sendKeepTrue(message.Inum);
							recording.m_keep = true;
						}
					}
					if (entry.getKey().toString().equalsIgnoreCase("CD8")) {
						String s_cd24 = entry.getValue().toString();
						if (s_cd24.equalsIgnoreCase("103") && !recording.m_isBlocked) {
							// m_log.info("CD8 match: %s", message.Inum);
							// sendKeepTrue(message.Inum);
						}
					}
				}
				recording.addAttributes(message.getParameters());
				Map<String, String> changed = recording.getChangedAttributes();
				if (0 == changed.size())
					return false;
				// String ANI = message.getStringParameterSafe("ANI");
				// this.m_log.debug("Process ANI:%s",ANI);
				m_activeRecordings.remove(message.Inum);
				m_activeRecordings.put(message.Inum, recording);
				m_log.debugHigh("Updating tracker with DN:%s ; SIPID:%s", message.Inum, recording.m_sipid);
				CallTracker.updateCall(message.Inum, recording);
				this.m_log.dev("Recording details: %s", recording.toString());
				if (recording.m_isBlocked == true) {
					sendKeepFalse(message.Inum);
				}
				return true;
			} else {

				return false;
			}
		} catch (Exception e) {
			m_log.error("Error processing Tagged message: %s", e.getMessage());
			return false;
		}
	}
	private void sendKeepTrue(String s_inum) {
		String msgString = "1 PRPTAG INUM:" + s_inum + ": KEEP:true: ROLLBACKREQUIRED:false:\033";
		m_log.debugHigh("Sending KEEP:true: for this recording:%s", msgString);
		sendMessage(msgString);
	}
	private void sendKeepFalse(String s_inum) {
		String msgString = "1 PRPTAG INUM:" + s_inum + ": KEEP:false: ROLLBACKREQUIRED:true:\033";
		m_log.debugHigh("Sending KEEP:false: for this recording:%s", msgString);
		sendMessage(msgString);
	}
	private void processUpdateMessage(NGAMessage message) {
		// m_log.debug("Process UPDATE message");
		try {
			if (null != message.getStringParameter("EXTENSION")) {
				m_extn.add(message.getStringParameter("EXTENSION"));
				// m_log.debug("Adding extension: %s",
				// message.getStringParameter("EXTENSION"));
				return;
			}
			m_log.dev("Update extension is null, ignoring");
		} catch (Exception e) {
			m_log.error("Error processing Update message: %s", e.getMessage());
		}
	}
	private void checkBlockedCall(final String s_sipid) {
		m_log.dev("Check Call history for sipid:%s", s_sipid);
		Runnable t_AP = new Thread() {
			public void run() {
				try {
					List<String> m_recs = CallTracker.getOldCalls(s_sipid);
					m_log.dev("Recording List has %s entries", m_recs.size());
					String s_inum = null;
					Iterator<String> m_it = m_recs.iterator();
					DeleteInum m_delInum = new DeleteInum();
					while (m_it.hasNext()) {
						s_inum = m_it.next();
						m_log.dev("checkBlockedCall has:%s on recorder:%s", s_inum, m_hostname);
						m_delInum.DelInum(s_inum, m_hostname, m_log);
					}
				} catch (Exception e1) {
					m_log.error("Exception getting list: %s", e1.getMessage());
				}
			}
		};
		Thread run_AP = new Thread(t_AP);
		run_AP.start();
	}
	public int getMajorVersion() {
		return this.m_ngaConnection == null ? 0 : this.m_ngaConnection.getMajorVersion();
	}
	public void sendTagMessage(String inum, Map<String, String> tagKVs) {
		String tagMsgVer = 11 > getMajorVersion() ? "3" : "6";
		String tagString = buildString(tagKVs);
		String cmdId = generateCommand();
		if (tagString.length() > 0) {
			sendMessage(tagMsgVer + " TAG CMDID:" + cmdId + ": INUM:" + inum + ": " + tagString + "\033");
			this.m_log.debug("TAG CMID:%s : INUM:%s:%s", cmdId, inum, tagString);
		}
	}
	public void sendIPTagMessage(String inum, Map<String, String> tagKVs) {
		try {
			// this.m_log.debug("Send IP Tag Message to inum:" +inum);
			String tagMsgVer = 11 > getMajorVersion() ? "3" : "6";
			String tagString = buildString(tagKVs);
			// this.m_log.debug("Tag String: " +tagString);
			if (tagString.length() > 0) {
				String msgString = tagMsgVer + " TAG INUM:" + inum + ": " + tagString + "\033";
				sendMessage(msgString);
				// m_log.debug("Sending:%s",msgString);
			}
		} catch (Exception e) {
			m_log.error("Error sending IPTAG command: %s", e.getMessage());
		}
	}
	public void sendIPExtTagMessage(String callid, String extn) {
		try {
			this.m_log.debug("Send IP ExtTag Message to callid:" + callid);
			String tagMsgVer = 11 > getMajorVersion() ? "3" : "6";
			String msgString = tagMsgVer + " EXTTAG CALLID:" + callid + ": " + "EXT:" + extn + ":" + "\033";
			// 6 EXTTAG CALLID:Cisco/1234: EXT:1000:
			// sendMessage(msgString);
			// m_log.debug("Sending:%s",msgString);
		} catch (Exception e) {
			m_log.error("Error sending Tag message: %s", e.getMessage());
		}
	}
	public void sendQuery() {
		try {
			this.m_ngaConnection.sendMessage("1 EXTENDEDINFO_QUERY\033");
			this.m_ngaConnection.sendMessage("1 REQUESTUPDATE\033");
		} catch (Exception e) {
			m_log.error("Error sending query for 'REQUESTEXTENSIONUPDATE' : %s", e.getMessage());
		}
	}
	public String buildString(Map<String, String> tagKVs) {
		StringBuilder tagString = new StringBuilder();
		if (tagKVs != null) {
			for (Map.Entry<String, String> kv : tagKVs.entrySet())
				tagString.append(HexUtils.encode(((String) kv.getKey()).toUpperCase())).append(":").append(HexUtils.encode((String) kv.getValue())).append(": ");
		}
		return tagString.toString();
	}

	private boolean sendMessage(String message) {
		if (this.m_ngaConnection == null)
			return false;
		// don't send any updates
		m_log.debugHigh("<%s> Sending: %s", this.m_hostname, message);
		this.m_ngaConnection.sendMessage(message);
		return true;
	}
	private int reserveCommand(int count) {
		if (count < 1) {
			return 0;
		}
		synchronized (this.m_commandLock) {
			int command = this.m_commandId;
			this.m_commandId += count;
			return command;
		}
	}
	private String generateCommand() {
		return this.m_hostname + reserveCommand(1);
	}
	private String findChannel(NGAMessage message) {
		String ret = null;
		String cn = message.ChannelRef;
		if (cn.length() == 0) {
			String inum = message.Inum;
			if (inum.length() == 0) {
				this.m_log.debug("Both inum and Channel are empty: %s", message.Command);
			}
		}
		return ret;
	}
	private String getExtension(String s_callRef) {
		String s_ret = "";
		return s_ret;
	}
	private Recording getRecording(String inum) {
		Recording ret = null;
		ret = m_activeRecordings.get(inum);
		if (null != ret) {
			this.m_log.debug("Recording found for inum: %s", inum);
		} else {
			this.m_log.debug("No recording found for inum: %s", inum);
		}
		return ret;
	}

	public boolean recorderDead() {
		m_log.debugHigh("<%s> request for recorder status, return %s", IPRole.this.m_hostname, IPRole.this.isDead);
		return isDead ? true : false;
	}
	private class CheckConnection implements Runnable {
		private CheckConnection() {
		}
		public void run() {
			m_log.debugHigh("Checking connection to recorder <%s>", IPRole.this.m_hostname);
			if (IPRole.this.m_ngaConnection != null) {
				if (!IPRole.this.m_ngaConnection.isAlive()) {
					m_log.error("Connection to recorder <%s> is not alive, reset", IPRole.this.m_hostname);
					IPRole.this.client.reset();
				}
				m_log.debugHigh("<%s> reports connected", IPRole.this.m_hostname);
			} else {
				m_log.error("Connection to recorder <%s> is null, reset", IPRole.this.m_hostname);
				IPRole.this.client.reset();
			}
			long m_l = (System.currentTimeMillis() - m_lastSeen);
			m_log.debugHigh("<%s> last seen at <%s> current time <%s> delta <%s>", m_hostname, m_lastSeen, System.currentTimeMillis(), m_l);
			/**
			 * if ((System.currentTimeMillis() - m_lastSeen) >90000) {
			 * m_log.error("<%s> recorder not seen, reset",
			 * IPRole.this.m_hostname); IPRole.this.isDead = true;
			 * m_log.error("<%s> recorder not seen setting isDead to: %s",
			 * IPRole.this.m_hostname, IPRole.this.isDead);
			 * IPRole.this.client.reset(); start(); }
			 **/
		}
	}
}
