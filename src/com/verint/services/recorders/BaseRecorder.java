package com.verint.services.recorders;

import java.util.List;
import java.util.Map;

import com.witness.common.threading.GlobalTimerWheel;
import com.witness.common.threading.TimerWheelCommand;

public class BaseRecorder {
	protected String m_serialNo;
	protected String m_hostname;
	protected String m_type;
	protected int m_id;
	protected int m_port = 29504;
	protected List<String> m_roles;
	protected IPRole ipclass;
	protected TimerWheelCommand m_command;

	public BaseRecorder(int id, String hostname, String serialNo, List<String> roles, Map<String, String> rec_settings) {
			//System.out.println("New base Recorder id: " + id);
			this.m_id = id;
			this.m_hostname = hostname;
			this.m_serialNo = serialNo;
			this.m_roles = roles;
			for (String s : roles) {
				addRoles(s);
			}
			for (Map.Entry entry : rec_settings.entrySet()) {
				if ("General_IPControlPort".equals(entry.getKey())) {
					m_port = Integer.parseInt((String) entry.getValue());
				}
			}

			//this.m_command = GlobalTimerWheel.getTimerWheel().submitRepeatedCommand(new CheckConnection(), 9000L, 5000L);
	}
	
	public boolean addRoles(String role) {
		if (("iprecorder".equalsIgnoreCase(role)) || ("ip_recorder".equalsIgnoreCase(role))) {
			ipclass = new IPRole(m_id, m_hostname);
		}
		if (("tdmrecorder".equalsIgnoreCase(role)) || ("tdm_recorder".equalsIgnoreCase(role))) {
			TdmRole tdmclass = new TdmRole(m_id, m_hostname);
		}
		return true;
	}
	
	private class CheckConnection implements Runnable {
		private CheckConnection() {
			}
		public void run() {
			/**System.out.println("<" +m_hostname +"> Base recorder connection check");
			System.out.println("Reports as: " +ipclass.recorderDead());
			if (ipclass.recorderDead()) {
				System.out.println("<" +m_hostname +"> IPRole reports dead");
				ipclass = null;
				ipclass = new IPRole(m_id, m_hostname);
			}
			**/
		}
	}
}
