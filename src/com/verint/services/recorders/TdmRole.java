package com.verint.services.recorders;

import java.util.HashMap;
import java.util.Map;

import com.verint.services.model.CallTracker;
import com.verint.services.model.Recording;
import com.witness.common.NGA.IComponentRegistration;
import com.witness.common.NGA.IProcessNGAMessage;
import com.witness.common.NGA.NGAConnection;
import com.witness.common.NGA.NGAConnection.ReasonLost;
import com.witness.common.NGA.NGAClient;
import com.witness.common.NGA.NGAMessage;
import com.witness.common.threading.GlobalTimerWheel;
import com.witness.common.threading.TimerWheelCommand;
import com.witness.common.util.HexUtils;
import com.witness.common.wdls.WitLogger;

public class TdmRole implements IComponentRegistration, IProcessNGAMessage
{
	private final WitLogger m_log = WitLogger.create("TdmRoleClass");
	private String m_hostname;
	private int m_id;
	private int m_port = 29502;
	private NGAConnection m_ngaConnection;
	private NGAClient client = null;
	private TimerWheelCommand m_command;
	private Object m_commandLock = new Object();
	private int m_commandId = 1;
	private int m_update = 1;
	private Map<String, Recording> m_activeRecordings = new HashMap();
	
	public TdmRole(int id, String hostname) {
		System.out.println("Creating TDM role for recorder id:" +id);
		this.m_id = id;
		this.m_hostname = hostname;
		start();
	}
	public boolean start() {
		this.m_log.info("Get connection to Recorder %s on port %s",m_hostname,m_port);
		if (this.client == null) {
			this.client = new NGAClient(m_hostname, m_port, 3, "IF", "IFServices", this);
			this.m_command = GlobalTimerWheel.getTimerWheel().submitRepeatedCommand(new CheckConnection(), 5000L, 5000L);
			return this.client.reset();
		}
		return true;
	}
	
	public void stop() {
		if (this.m_command != null)
			this.m_command.cancelCommand();
		    this.m_command = null;
		    if (this.m_ngaConnection != null)
		    	this.m_ngaConnection.close();
		    this.m_ngaConnection = null;

		    this.client.Shutdown();
		}

	public void UnregisterComponent(NGAConnection connection, NGAConnection.ReasonLost reason){
		this.m_ngaConnection = null;
	}

	public void RegisterComponent(NGAConnection connection) {
	    this.m_log.debug("Register listener:%s",connection.toString());
	    this.m_ngaConnection = connection;
	    connection.setCallback(this);
	}

	public void checkForBackup(int arg0)
	{
	}

	 public void ProcessNGAMessage(NGAMessage m_message) {
		 this.m_log.debug("Process NGA message: %s",m_message.Command );
		    String cmd = m_message.Command;
		    if ("TAGGED".equalsIgnoreCase(cmd)) {
		    	processTagged(m_message);
		    }
		    else if ("STARTED".equalsIgnoreCase(cmd)) {
		    	processStarted(m_message);
		    }
		    else if("STOPPED".equalsIgnoreCase(cmd)) {
		    	processStopped(m_message);
		    }
		    else if ("HELLO".equalsIgnoreCase(cmd))
		    	processHello(m_message);
		    else if ("UPDATE".equalsIgnoreCase(cmd)){
			    this.m_log.debug("Update:%s", m_update);
		    }
		    if ("EXTENDEDINFO_RESPONSE".equalsIgnoreCase(cmd))
		    	processExtendedInfoResponse(m_message);
	 }
	 private void processExtendedInfoResponse(NGAMessage m_message) {
		 this.m_log.debug("Received EXTENDEDINFO_RESPONSE");
	 }
	 private void processHello(NGAMessage message) {
		 sendQuery();
		 this.m_log.info("Process HELLO message: " +message.getHost());
	 }
	 private void processStarted(NGAMessage message) {
		 this.m_log.debug("Process StartedMessage: %s",message.Command);
		 String Channel = findChannel(message);
		 Recording recording = new Recording(message.Inum, m_hostname, "Audio");
		 this.m_activeRecordings.put(message.Inum, recording);
		 String callRef = message.getCallRef();
		 this.m_log.info("STARTED: CallRef %s", callRef);
	 }
	 private void processStopped(NGAMessage message) {
		 this.m_log.debug("Process StoppedMessage: %s",message.Command);
		 String Channel = findChannel(message);
		 Recording recording = this.getRecording(message.Inum);
		 this.m_activeRecordings.remove(message.Inum);
		 String callRef = message.getCallRef();
		 this.m_log.info("STOPPED: CallRef %s", callRef);
	 }
	 private boolean processTagged(NGAMessage message) {
		 this.m_log.debug("Process Tagged Message");
		 Recording recording = this.getRecording(message.Inum);
		 this.m_log.info("Recording details: %s", recording.toString());
		 for (Map.Entry entry : message.getParameters().entrySet()){
			 System.out.println("pt:" +entry.getKey().toString() + " val:" +entry.getValue().toString());
			 //return false;
			 if (entry.getKey().toString().equalsIgnoreCase("CLI")){
				 m_log.debug("Event Type CLI Received Update Tagging");
				 String cp = entry.getValue().toString();
				 m_log.debug("Match CLI: %s to VoxSmart users" ,cp);

			 }
		 }
		 recording.addAttributes(message.getParameters());
		 Map changed = recording.getChangedAttributes();
		 if (0 == changed.size()) return false;
		 //String ANI = message.getStringParameterSafe("ANI");
		 //this.m_log.debug("Process ANI:%s",ANI);
		 m_activeRecordings.remove(message.Inum);
		 m_activeRecordings.put(message.Inum, recording);
		 return true;
	 }
	 private boolean updateTagging(String value) {
		 return true;
	 }
	 public int getMajorVersion() {
		 return this.m_ngaConnection == null ? 0 : this.m_ngaConnection.getMajorVersion();
	 }
	 public void sendTagMessage(String inum, Map<String, String> tagKVs) {
		 String tagMsgVer = 11 > getMajorVersion() ? "3" : "6";
		 String tagString = buildString(tagKVs);
		 String cmdId = generateCommand();
		 if (tagString.length() > 0) {
			 sendMessage(tagMsgVer + " TAG CMDID:" + cmdId + ": INUM:" + inum + ": " + tagString + "\033");
			 this.m_log.info("TAG CMID:%s : INUM:%s:%s",cmdId,inum,tagString);
		 }
	 }
	public void sendIPTagMessage(String inum, Map<String, String> tagKVs) {
		this.m_log.debug("Send IP Tag Message to inum:" +inum);
		String tagMsgVer = 11 > getMajorVersion() ? "3" : "6";
		String tagString = buildString(tagKVs);
		this.m_log.debug("Tag String: " +tagString);
		if (tagString.length() > 0) {
			String msgString = tagMsgVer + " TAG INUM:" + inum + ": " + tagString + "\033";
			sendMessage(msgString);
		}
	}
	public void sendQuery() {
		String cmdId = generateCommand();
		this.m_log.debug("1 EXTENDEDINFO_QUERY CMDID:%s",cmdId);
		sendMessage("1 EXTENDEDINFO_QUERY CMDID:" +cmdId+":\033");
	}
	 public String buildString(Map<String, String> tagKVs) {
		 StringBuilder tagString = new StringBuilder();
		 if (tagKVs != null) {
			 for (Map.Entry kv : tagKVs.entrySet())
				 tagString.append(HexUtils.encode(((String)kv.getKey()).toUpperCase())).append(":").append(HexUtils.encode((String)kv.getValue())).append(": ");
		 	}
		 	return tagString.toString();
	 }

	 private boolean sendMessage(String message) {
		 if (this.m_ngaConnection == null) return false;
		 	this.m_ngaConnection.sendMessage(message);
		 	return true;
	 }
	 private int reserveCommand(int count) {
		 if (count < 1) {
			 return 0;
		 }
		 synchronized (this.m_commandLock) {
			 int command = this.m_commandId;
			 this.m_commandId += count;
			 return command;
		 }
	}
	private String generateCommand() {
		return "rs-dude" + reserveCommand(1);
	}
	private String findChannel(NGAMessage message) {
		String ret = null;
		String cn = message.ChannelRef;
		if (cn.length() ==0) {
			String inum = message.Inum;
			if (inum.length() ==0) {
				this.m_log.debug("Both inum and Channel are empty: %s",message.Command);
			}
		}
		return ret;
	}
	private Recording getRecording(String inum) {
		Recording ret = null;
		ret = m_activeRecordings.get(inum);
		if (null != ret) {
			this.m_log.debug("Recording found for inum: %s", inum);
		}else{
			this.m_log.debug("No recording found for inum: %s",inum);
		}
		return ret;
	}
	private class CheckConnection implements Runnable {
		private CheckConnection() {
			}
		public void run() {
			//System.out.println("Check connection for recorder");
			if (TdmRole.this.m_ngaConnection != null) {
				//System.out.println("Its not null");
				if (!TdmRole.this.m_ngaConnection.isAlive()) {
					System.out.println("its not alive, reset");
					TdmRole.this.client.reset();
				}
			} else{
				System.out.println("it is null, reset");
				TdmRole.this.client.reset();
			}
			}
		}
}

