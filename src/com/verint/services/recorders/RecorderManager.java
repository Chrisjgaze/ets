package com.verint.services.recorders;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.verint.services.adapters.Genesys.GenesysAdapter;

public class RecorderManager {
	private Map<Integer, BaseRecorder> m_recorders = new HashMap();

	public RecorderManager() {
	}

	public void addRecorder(final int id, final String hostname, final String serialNo, final List<String> roles, final Map<String, String> rec_settings) {

		/**
		java.lang.Runnable treatEvent = new java.lang.Runnable() {
			public void run() {
				try {
					// System.out.println("Creating new recorder in recorder manager:"
					// + hostname);
					final BaseRecorder recorder = new BaseRecorder(id, hostname, serialNo, roles, rec_settings);
				} catch (Exception e) {

				}
			}
		};
		java.lang.Thread doTreatment = new java.lang.Thread(treatEvent);
		doTreatment.start();
		**/
		BaseRecorder recorder = new BaseRecorder(id, hostname, serialNo, roles, rec_settings);
	}
	public BaseRecorder getRecorder(int id) {
		return m_recorders.get(id);
	}
}
