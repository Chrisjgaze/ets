package com.verint.services.recorders;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import com.witness.common.util.XMLHelper;
import com.verint.services.utilites.ConfigurationUtility;

public class RecorderParser {
	private static boolean m_isServerXml = true;
	private static RecorderManager recordermanager = new RecorderManager();
	public RecorderParser(){
		start();
	}
	public void start(){
		String filename = "Servers.xml";
		File file = ConfigurationUtility.getConfigurationFile(filename);
	    Document doc = XMLHelper.readXMLFile(filename, file);
	    //System.out.println("Get file: ");
	    if (null == doc)
	    	return;
	    //System.out.println("Got file");
	       Element root = doc.getDocumentElement();
	       Element enterprise = XMLHelper.getElementByName(root, "Enterprise");
	       handleSite(enterprise, "");
	}
	private void handleSite(Element site, String indent) {
		//System.out.println("handling site: " +site.getAttribute("Name"));
		String name = site.getAttribute("Name");
		if ((null == name) || (0 == name.length())) name = site.getAttribute("name");
			    
		for (Element sg : XMLHelper.getElementsByName(site, "SiteGroup")) {
			handleSite(sg, indent + "  ");
		}
		for (Element s : XMLHelper.getElementsByName(site, "Site")) {
			handleSite(s, indent + "  ");
		}
		for (Element n : XMLHelper.getElementsByName(site, this.m_isServerXml ? "Server" : "Node")){
			handleServer(n, indent + "  ");
		}
			
	}
	private void handleRecorderNodeRole() {
		
	}
	private void handleServer(Element node, String indent) {
		List<String> roles = new ArrayList();
		Map<String, String> rec_settings = new HashMap();
		int id = XMLHelper.getAttributeInt(node, "Identifier");
		String hostname = node.getAttribute("Hostname");
		String serialNo = node.getAttribute("SerialNumber");
		boolean has_iprole = false;
		boolean has_tdmrole = false;
		Element settings = XMLHelper.getElementByName(node, "Settings");
	    if (null != settings) {
	        for (Element setting : XMLHelper.getElementsByName(settings, "Setting"))
	          rec_settings.put(setting.getAttribute("Name"), setting.getAttribute("Value"));
	      }
		//System.out.println("Found: " +hostname +", " +id +", " +serialNo);
		for (Element n : XMLHelper.getElementsByName(node, m_isServerXml ? "ServerRole" : "NodeRole")) {
			String role = handleServerRole(n, indent + "  ", hostname);
			if (("iprecorder".equalsIgnoreCase(role)) || ("ip_recorder".equalsIgnoreCase(role)))
				roles.add(role);
			if (("tdmrecorder".equalsIgnoreCase(role)) || ("tdm_recorder".equalsIgnoreCase(role)))
				roles.add(role);
		}
		for (String role : roles) {
			//System.out.println("role:" +role);
		}
		for (String role : roles){
	    	has_iprole = ("iprecorder".equalsIgnoreCase(role)) || ("ip_recorder".equalsIgnoreCase(role));
	    	has_tdmrole = ("tdmrecorder".equalsIgnoreCase(role)) || ("tdm_recorder".equalsIgnoreCase(role));
		}
		//System.out.println("has ip:" + has_iprole);
	    if ((has_iprole) || (has_tdmrole))
	    	recordermanager.addRecorder(id, hostname, serialNo, roles, rec_settings);

	}
	private static String handleServerRole(Element nodeRole, String indent, String hostname) {
		String role = nodeRole.getAttribute(m_isServerXml ? "Name" : "Role");
		if ("IP_RECORDER".equalsIgnoreCase(role))
			return role;
		else if("TDM_RECORDER".equalsIgnoreCase(role))
			return role;
		return null;
	}
}

